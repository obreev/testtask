﻿using AiForms.Renderers.Droid;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Android.Views;
using Autofac;
using Rg.Plugins.Popup;
using Rg.Plugins.Popup.Services;
using TouchEffect.Android;
using Xamarin;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TestTask.Droid
{
    [Activity(Label = "TestTask", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize )]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
	        TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            Popup.Init(this, savedInstanceState);

            Platform.Init(this, savedInstanceState);
            Forms.Init(this, savedInstanceState);
            FormsMaterial.Init(this, savedInstanceState);
            FormsGoogleMaps.Init(this, savedInstanceState);

            SettingsViewInit.Init();
            TouchEffectPreserver.Preserve();

            var containerBuilder = new ContainerBuilder();
            DoRegisterPlatformServices(containerBuilder);

            UpdateStatusBar();

            LoadApplication(new App(containerBuilder));
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void DoRegisterPlatformServices(ContainerBuilder containerBuilder)
        {
        }

        private void UpdateStatusBar()
        {
	        if (Build.VERSION.SdkInt < BuildVersionCodes.Lollipop)
		        return;

	        if (Window == null)
		        return;

            // for covering the full screen in android..
            Window.SetFlags(WindowManagerFlags.LayoutNoLimits, WindowManagerFlags.LayoutNoLimits);

            // clear FLAG_TRANSLUCENT_STATUS flag:
            Window.ClearFlags(WindowManagerFlags.TranslucentStatus);

            Window.SetStatusBarColor(Android.Graphics.Color.Transparent);

            Window.DecorView.SystemUiVisibility = (StatusBarVisibility)SystemUiFlags.LightStatusBar;
        }

        public override void OnBackPressed()
        {
	        if (Popup.SendBackPressed(base.OnBackPressed))
	        {
		        MainThread.BeginInvokeOnMainThread(() =>
		        {
			        PopupNavigation.Instance.PopAsync();
		        });
	        }
	        else
	        {
		        // Do something if there are not any pages in the `PopupStack`
	        }
        }
    }
}
