﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using TestTask.Droid.Renderers;
using TestTask.Views.Base;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BaseContentPage), typeof(BaseContentPageRenderer))]

namespace TestTask.Droid.Renderers
{
	public class BaseContentPageRenderer : PageRenderer
	{
		// ReSharper disable PossibleNullReferenceException
        public BaseContentPageRenderer(Context context) : base(context)
		{

		}

        private BaseContentPage CurrentPage => (BaseContentPage) Element;


        #region Overrides of PageRenderer

        protected override void OnAttachedToWindow()
        {
	        CurrentPage.Appearing += CurrentPageOnAppearing;
        }

        protected override void OnDetachedFromWindow()
        {
	        CurrentPage.Appearing -= CurrentPageOnAppearing;
        }

        private void CurrentPageOnAppearing(object sender, EventArgs e)
        {
	        var activity = (Activity)Context;
	        if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
	        {
		        SetStatusBarStyle(activity.Window, CurrentPage.StatusBarStyle);
	        }
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            var activity = (Activity) Context;

            Thickness systemPadding;

            var decoreView = activity.Window.DecorView;
            var decoreHeight = decoreView.Height;
            var decoreWidht = decoreView.Width;

            var visibleRect = new Android.Graphics.Rect();

            decoreView.GetWindowVisibleDisplayFrame(visibleRect);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                var screenRealSize = new Android.Graphics.Point();
                activity.WindowManager.DefaultDisplay.GetRealSize(screenRealSize);

                var windowInsets = RootWindowInsets;

                systemPadding = new Thickness
                {
                    Left = Context.FromPixels(windowInsets.StableInsetLeft),
                    Top = Context.FromPixels(windowInsets.StableInsetTop),
                    Right = Context.FromPixels(windowInsets.StableInsetRight),
                    Bottom = Context.FromPixels(windowInsets.StableInsetBottom)
                };
            }
            else
            {
                var screenSize = new Android.Graphics.Point();
                activity.WindowManager.DefaultDisplay.GetSize(screenSize);

                systemPadding = new Thickness
                {
                    Left = Context.FromPixels(visibleRect.Left),
                    Top = Context.FromPixels(visibleRect.Top),
                    Right = Context.FromPixels(decoreWidht - visibleRect.Right),
                    Bottom = Context.FromPixels(decoreHeight - visibleRect.Bottom)
                };
            }

            CurrentPage.SetValue(BaseContentPage.SystemPaddingProperty, systemPadding);

            if (changed)
                CurrentPage.Layout(new Rectangle(Context.FromPixels(l), Context.FromPixels(t), Context.FromPixels(r), Context.FromPixels(b)));
            else
	            CurrentPage.ForceLayout();

            base.OnLayout(changed, l, t, r, b);
        }

        #endregion


        private void SetStatusBarStyle(Window window, StatusBarStyle statusBarStyle)
        {
	        switch (statusBarStyle)
	        {
		        case StatusBarStyle.DarkText:
			        window.DecorView.SystemUiVisibility = (StatusBarVisibility)SystemUiFlags.LightStatusBar;
			        break;
		        case StatusBarStyle.WhiteText:
			        window.DecorView.SystemUiVisibility = 0;
			        break;
	        }
        }
	}
}
