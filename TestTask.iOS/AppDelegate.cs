﻿using AiForms.Renderers.iOS;
using Autofac;
using Foundation;
using Rg.Plugins.Popup;
using TouchEffect.iOS;
using UIKit;
using Xamarin;
using Xamarin.Forms;

namespace TestTask.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register(nameof(AppDelegate))]
    public partial class AppDelegate : Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //

        private const string GoogleMapsApiIdentifier = "AIzaSyAdAlSxCY464PTTlrzegWKVRRb5tD_zPDA";

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
	        Popup.Init();

            Forms.Init();
            FormsMaterial.Init();
            FormsGoogleMaps.Init(GoogleMapsApiIdentifier);

            SettingsViewInit.Init();
            TouchEffectPreserver.Preserve();

            var containerBuilder = new ContainerBuilder();
            DoRegisterPlatformServices(containerBuilder);


            LoadApplication(new App(containerBuilder));

            return base.FinishedLaunching(app, options);
        }

        private void DoRegisterPlatformServices(ContainerBuilder containerBuilder)
        {
        }
    }
}
