﻿using TestTask.iOS.Renderers;
using TestTask.Views.Base;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BaseContentPage), typeof(BaseContentPageRenderer))]

namespace TestTask.iOS.Renderers
{
	public class BaseContentPageRenderer : PageRenderer
	{
		private BaseContentPage CurrentPage => (BaseContentPage)Element;

		public override void ViewDidLayoutSubviews()
		{
			UpdateSize();
			base.ViewDidLayoutSubviews();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			SetStatusBarStyle(CurrentPage.StatusBarStyle);
		}

		private void UpdateSize()
		{
			var currentElement = CurrentPage;

			if (View?.Superview?.Frame == null || currentElement == null)
				return;

			var superviewFrame = View.Superview.Frame;
			var applactionFrame = UIScreen.MainScreen.ApplicationFrame;
			 
			var systemPadding = new Thickness
			{
				Left = applactionFrame.Left,
				Top = applactionFrame.Top,
				Right = applactionFrame.Right - applactionFrame.Width - applactionFrame.Left,
				Bottom = applactionFrame.Bottom - applactionFrame.Height - applactionFrame.Top
			};

			currentElement.BatchBegin();

			CurrentPage.SetValue(BaseContentPage.SystemPaddingProperty, systemPadding);
			SetElementSize(new Size(superviewFrame.Width, superviewFrame.Height));

			currentElement.BatchCommit();
		}

		private void SetStatusBarStyle(StatusBarStyle statusBarStyle)
		{
			switch (statusBarStyle)
			{
				case StatusBarStyle.DarkText:
					UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.DarkContent, true);
					UIApplication.SharedApplication.SetStatusBarHidden(false, true);
					break;
				case StatusBarStyle.WhiteText:
					UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, true);
					UIApplication.SharedApplication.SetStatusBarHidden(false, true);
					break;
				case StatusBarStyle.Hidden:
					UIApplication.SharedApplication.SetStatusBarHidden(true, true);
					break;
				default:
					UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.Default, true);
					UIApplication.SharedApplication.SetStatusBarHidden(false, true);
					break;
			}

			SetNeedsStatusBarAppearanceUpdate();
		}
	}
}
