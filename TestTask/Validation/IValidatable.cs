﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace TestTask.Validation
{
	public interface IValidatable<T> : INotifyPropertyChanged
	{
		List<IValidationRule<T>> Validations { get; }

		ObservableCollection<string> Errors { get; set; }

		bool Validate();

		bool IsValid { get; set; }
	}
}
