﻿
namespace TestTask.Validation.Rules
{
    public class IsLenghtValidRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        public int MinimunLenght { get; set; }

        public int MaximunLenght { get; set; }

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }

            return value is string str && str.Length > MinimunLenght && str.Length <= MaximunLenght;
        }
    }
}
