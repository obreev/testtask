﻿
namespace TestTask.Validation.Rules
{
    public class IsValidEmailRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        public bool Check(T value)
        {
            try
            {
                var email = new System.Net.Mail.MailAddress($"{value}");
                return email.Address == $"{value}";
            }
            catch
            {
                return false;
            }
        }
    }
}
