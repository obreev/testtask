﻿using System;

namespace TestTask.Validation.Rules
{
    public class HasValidAgeRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        public bool Check(T value)
        {
	        if (!(value is DateTime bday))
		        return false;

	        var today = DateTime.Today;
            var age = today.Year - bday.Year;
            return age >= 18;

        }
    }
}
