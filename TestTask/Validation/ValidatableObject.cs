﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TestTask.Helpers.Mvvm;

namespace TestTask.Validation
{
	public class ValidatableObject<T> : ObservableObject, IValidatable<T>
	{
		public List<IValidationRule<T>> Validations { get; } = new List<IValidationRule<T>>();

		private ObservableCollection<string> _errors = new ObservableCollection<string>();
		public ObservableCollection<string> Errors
		{
			get => _errors;
			set => SetProperty(ref _errors, value);
		}

		private T _value;
		public T Value
		{
			get => _value;
			set => SetProperty(ref _value, value);
		}

		private bool _isValid = true;
		public bool IsValid
		{
			get => _isValid;
			set => SetProperty(ref _isValid, value);
		}

		public virtual bool Validate()
		{
			Errors.Clear();

			var errors = Validations.Where(v => !v.Check(Value)).Select(v => v.ValidationMessage);

			Errors = null;
			Errors = new ObservableCollection<string>(errors.ToList());
			IsValid = !Errors.Any();

			return IsValid;
		}
		public override string ToString()
		{
			return $"{Value}";
		}
	}
}
