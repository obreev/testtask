﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TestTask.Helpers.Mvvm;

namespace TestTask.Validation
{
	public class ValidatablePair<T> : ObservableObject, IValidatable<ValidatablePair<T>>
	{
		public List<IValidationRule<ValidatablePair<T>>> Validations { get; } = new List<IValidationRule<ValidatablePair<T>>>();

		public bool IsValid { get; set; } = true;

		public ObservableCollection<string> Errors { get; set; } = new ObservableCollection<string>();

		public ValidatableObject<T> Item1 { get; set; } = new ValidatableObject<T>();

		public ValidatableObject<T> Item2 { get; set; } = new ValidatableObject<T>();


		public bool Validate()
		{
			var item1IsValid = Item1.Validate();
			var item2IsValid = Item2.Validate();

			if (item1IsValid && item2IsValid)
			{
				Errors.Clear();

				var errors = Validations.Where(v => !v.Check(this)).Select(v => v.ValidationMessage);

				Errors = new ObservableCollection<string>(errors.ToList());
				Item2.Errors = Errors;
				Item2.IsValid = !Errors.Any();
			}

			IsValid = !Item1.Errors.Any() && !Item2.Errors.Any();

			return IsValid;
		}
	}
}
