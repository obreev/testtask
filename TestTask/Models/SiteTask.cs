﻿using System;

namespace TestTask.Models
{
	public class SiteTask
	{
		public string Id { get; set; }

		public User User { get; set; }

		public Site Site { get; set; }

		public DateTime DateTime { get; set; }

		public string DateTimeFormatted => DateTime.ToString("dd MMM yyyy hh:mm");

		public DateTime DateTime2 { get; set; }

		public string DateTime2Formatted => DateTime.ToString("t");

		public string Title { get; set; }

		public string Message { get; set; }
	}
}
