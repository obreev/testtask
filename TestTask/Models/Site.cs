﻿
namespace TestTask.Models
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>id:guid, title:string:, fullAddress:string, status:int(0,1,2), notices:int, cameras:int, zones:int, requests:int, contacts:int</remarks>
	public class Site
	{
		public string Id { get; set; }

		public string Title { get; set; }

		public string FullAddress { get; set; }

		public int Status { get; set; }

		public int Notices { get; set; }

		public int Cameras { get; set; }

		public int Zones { get; set; }

		public int Requests { get; set; }

		public int Contacts { get; set; }

		public Coordinate Coordinate { get; set; }

		public bool SetupRequired { get; set; }
	}
}
