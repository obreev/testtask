﻿using System;

namespace TestTask.Models
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>id:guid, firstName:string, lastName:string, avatarUrl:string, jobTitle:string, companyName:string, phone:string, email:string, preferedContactMethod:int</remarks>
	public class User
	{
		public Guid Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string FullNameInitials => $"{FirstName?.Substring(0, 1)} {LastName?.Substring(0, 1)}";

		public string FullName => $"{FirstName} {LastName}";

		public string AvatarUrl { get; set; }

		public string JobTitle { get; set; }

		public string CompanyName { get; set; }

		public string Phone { get; set; }

		public string Email { get; set; }

		public int PreferedContactMethod { get; set; }
	}
}
