﻿#nullable enable

using System;
using System.Runtime.CompilerServices;

namespace TestTask.Services.Logging
{
	public static class LogExtensions
	{
		public static void Verbose(this ILog log, object? caller, string message = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.Write(LogLevels.Verbose, caller, message, callerMemberName, lineNumber);
		}

		public static void Debug(this ILog log, object? caller, string message = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.Write(LogLevels.Debug, caller, message, callerMemberName, lineNumber);
		}

		public static void Info(this ILog log, object? caller, string message = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.Write(LogLevels.Info, caller, message, callerMemberName, lineNumber);
		}

		public static void Warning(this ILog log, object? caller, string message = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.Write(LogLevels.Warning, caller, message, callerMemberName, lineNumber);
		}

		public static void Error(this ILog log, object? caller, string message = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.Write(LogLevels.Error, caller, message, callerMemberName, lineNumber);
		}

		public static void Error(this ILog log, object? caller, string message, Exception? e, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.WriteException(LogLevels.Error, caller, message, e, callerMemberName, lineNumber);
		}

		public static void Error(this ILog log, object? caller, Exception? e, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.WriteException(LogLevels.Error, caller, e, callerMemberName, lineNumber);
		}

		public static void Fatal(this ILog log, object? caller, string message = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.Write(LogLevels.Fatal, caller, message, callerMemberName, lineNumber);
		}

		public static void Fatal(this ILog log, object? caller, string message, Exception? e, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.WriteException(LogLevels.Fatal, caller, message, e, callerMemberName, lineNumber);
		}

		public static void Fatal(this ILog log, object? caller, Exception? e, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0)
		{
			log.WriteException(LogLevels.Fatal, caller, e, callerMemberName, lineNumber);
		}
	}
}
