﻿#nullable enable

using System;
using System.Diagnostics;
using System.Text;

namespace TestTask.Services.Logging
{
	public class Log : ILog
	{
		private readonly StringBuilder _logStorage = new StringBuilder();

		#region Implementation of ILog

		public void Write(LogLevels logLevel, object? caller, string? message, string callerMemberName, int lineNumber)
		{
			var line = $"{DateTime.UtcNow:T}\t{logLevel}\t{caller?.GetType().Name}::{callerMemberName}:{lineNumber}\t{message}";
			_logStorage.Append(line);
			_logStorage.AppendLine();

#if DEBUG
			Debug.WriteLine(line);
#endif
		}

		public void WriteException(LogLevels logLevel, object? caller, string message, Exception? e,
			string callerMemberName, int lineNumber)
		{
			Write(logLevel, caller, message, callerMemberName, lineNumber);
			WriteException(logLevel, caller, e, callerMemberName, lineNumber);
		}

		public void WriteException(LogLevels logLevel, object? caller, Exception? e, string callerMemberName,
			int lineNumber)
		{
			Write(logLevel, caller, e?.ToString(), callerMemberName, lineNumber);
		}

		public string GetSessionLogs()
		{
			return _logStorage.ToString();
		}

		public void ClearSessionLogs()
		{
			_logStorage.Clear();
		}

		#endregion
	}
}
