﻿#nullable enable

using System;

namespace TestTask.Services.Logging
{
	public interface ILog
	{
		void Write(LogLevels logLevel, object? caller, string? message, string callerMemberName, int lineNumber);

		void WriteException(LogLevels logLevel, object? caller, string message, Exception? e, string callerMemberName,
			int lineNumber);

		void WriteException(LogLevels logLevel, object? caller, Exception? e, string callerMemberName, int lineNumber);

		string GetSessionLogs();

		void ClearSessionLogs();
	}
}
