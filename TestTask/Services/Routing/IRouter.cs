﻿using System.Threading.Tasks;
using TestTask.ViewModels.Base;

namespace TestTask.Services.Routing
{
	public interface IRouter
	{
		Task<TViewModel> OpenNavigationPageAsync<TViewModel>(bool animate = false, params object[] parameters)
			where TViewModel : class, IBasePageViewModel;

		Task CloseNavigationPageAsync(bool animate = false);


		Task<TViewModel> OpenPopupPageAsync<TViewModel>(bool animate = false, params object[] parameters)
			where TViewModel : class, IBasePageViewModel;

		Task ClosePopupAsync(bool animate = false);
	}
}
