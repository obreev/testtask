﻿using System.Threading.Tasks;
using Autofac;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using TestTask.Services.Logging;
using TestTask.ViewModels.Base;
using TestTask.Views.Base;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace TestTask.Services.Routing
{
	public class Router : BaseService, IRouter
	{
		private readonly IMainThread _mainThread;
		private readonly INavigation _navigation;

		public Router(IMainThread mainThread,
			INavigation navigation)
		{
			_mainThread = mainThread;
			_navigation = navigation;
		}


		#region Implementation of IRouter

		public async Task<TViewModel> OpenNavigationPageAsync<TViewModel>(bool animate = false, params object[] parameters)
			where TViewModel : class, IBasePageViewModel
		{
			_log.Debug(this, $"Process navigation to {nameof(TViewModel)}");

			// получаем экземпляр TViewModel
			var associatedPageViewModel = IoC.Container.Resolve<TViewModel>();
			// инициализируем TViewModel в отдельном потоке с передачей параметров
			await Task.Run(() =>
			{
				associatedPageViewModel.InitializeAsync(parameters);
			}).ConfigureAwait(false);

			// получаем экземпляр Page и совокупляем ее с TViewModel
			var associatedPage = IoC.Container.ResolveKeyed<Page>(typeof(TViewModel)) as BaseContentPage<TViewModel>;
			associatedPage?.SetBindingContext(associatedPageViewModel);

			// навигация
			await _mainThread.InvokeOnMainThreadAsync(() =>
			{
				_navigation.PushAsync(associatedPage);
			});

			return associatedPageViewModel;
		}

		public async Task CloseNavigationPageAsync(bool animate = false)
		{
			_log.Debug(this);

			await _mainThread.InvokeOnMainThreadAsync(() =>
			{
				_navigation.PopAsync();
			}).ConfigureAwait(false);
		}

		public async Task<TViewModel> OpenPopupPageAsync<TViewModel>(bool animate = false, params object[] parameters)
			where TViewModel : class, IBasePageViewModel
		{
			_log.Debug(this, $"Process navigation to {nameof(TViewModel)}");

			// получаем экземпляр TViewModel
			var associatedPageViewModel = IoC.Container.Resolve<TViewModel>();
			// инициализируем TViewModel в отдельном потоке с передачей параметров
			await Task.Run(() =>
			{
				associatedPageViewModel.InitializeAsync(parameters);
			}).ConfigureAwait(false);

			// получаем экземпляр PopupPage и совокупляем ее с TViewModel
			var associatedPage = IoC.Container.ResolveKeyed<PopupPage>(typeof(TViewModel));
			associatedPage.BindingContext = associatedPageViewModel;

			// Popup навигация 
			await _mainThread.InvokeOnMainThreadAsync(() =>
			{
				PopupNavigation.Instance.PushAsync(associatedPage, animate);
			}).ConfigureAwait(false);

			return associatedPageViewModel;
		}

		public async Task ClosePopupAsync(bool animate = false)
		{
			_log.Debug(this);

			await _mainThread.InvokeOnMainThreadAsync(() =>
			{
				PopupNavigation.Instance.PopAsync(animate);
			}).ConfigureAwait(false);
		}

		#endregion
	}
}
