﻿using System.Collections.Generic;
using TestTask.Models;

namespace TestTask.Services.Sites
{
	public interface ISiteProvider
	{
		List<Site> GetSites(string searchText);
	}
}
