﻿using System;
using System.Collections.Generic;
using TestTask.Models;

namespace TestTask.Services.Sites
{
	public class FakeSiteProvider : ISiteProvider
	{
		#region Implementation of ISiteProvider

		public List<Site> GetSites(string searchText)
		{
			var rnd = new Random();

			return new List<Site>
			{

				new Site
				{
					Id = "1", 
					Title = "New Sushi Palace", 
					FullAddress = "1702 Middle Country Rd Centereach, NY 11720, USA", 
					Status = 1, 
					Notices = rnd.Next(0, 10), 
					Cameras = rnd.Next(0, 10), 
					Zones = rnd.Next(0, 3), 
					Requests = rnd.Next(0, 5), 
					Contacts = rnd.Next(0, 5),
                    Coordinate = new Coordinate
                    {
                        Latitude = 40.859839,
                        Longitude = -73.072474
                    },
                    SetupRequired = false
				},
				new Site
				{
					Id = "2", 
					Title = "Buffalo Wild Wings", 
					FullAddress = "1986 Middle Country Rd, Centereach, NY 11720, USA", 
					Status = 0, 
					Notices = rnd.Next(0, 10), 
					Cameras = rnd.Next(0, 10), 
					Zones = rnd.Next(0, 3), 
					Requests = rnd.Next(0, 5),
					Contacts = rnd.Next(0, 5),
					Coordinate = new Coordinate
					{
						Latitude = 40.857263,
						Longitude = -73.081787
                    },
					SetupRequired = false
                },
				new Site
				{
					Id = "3",
					Title = "Kup's Auto Spa",
					FullAddress = "121 Mark Tree Rd Centereach, NY 11720, USA",
					Status = 1,
					Notices = rnd.Next(0, 10),
					Cameras = rnd.Next(0, 10), 
					Zones = rnd.Next(0, 3), 
					Requests = rnd.Next(0, 5), 
					Contacts = rnd.Next(0, 5),
					Coordinate = new Coordinate
					{
						Latitude = 40.862104,
						Longitude = -73.084337
					},
					SetupRequired = true
                },
				new Site
				{
					Id = "4", 
					Title = "Anthony and Kristin’s", 
					FullAddress = "82 Oak St, Centereach, NY 11720, USA", 
					Status = 2, 
					Notices = rnd.Next(0, 10), 
					Cameras = rnd.Next(0, 10), 
					Zones = rnd.Next(0, 3), 
					Requests = rnd.Next(0, 5), 
					Contacts = rnd.Next(0, 5),
					Coordinate = new Coordinate
					{
						Latitude = 40.865271,
						Longitude = -73.098681
					},
					SetupRequired = false
				},
				new Site
				{
					Id = "5", 
					Title = "Vajiradhammapadip Temple Ltd.", 
					FullAddress = "110 Rustic Rd Centereach, NY 11720, USA", 
					Status = 1, 
					Notices = rnd.Next(0, 10), 
					Cameras = rnd.Next(0, 10), 
					Zones = rnd.Next(0, 3), 
					Requests = rnd.Next(0, 5), 
					Contacts = rnd.Next(0, 5),
					Coordinate = new Coordinate
					{
						Latitude = 40.852826,
						Longitude = -73.09418
					},
					SetupRequired = false
				},
				new Site
				{
					Id = "6", 
					Title = "Curam House", 
					FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", 
					Status = 1, 
					Notices = rnd.Next(0, 10), 
					Cameras = rnd.Next(0, 10), 
					Zones = rnd.Next(0, 3), 
					Requests = rnd.Next(0, 5), 
					Contacts = rnd.Next(0, 5),
					Coordinate = new Coordinate
					{
						Latitude = 40.774736,
						Longitude = -73.530866
					},
					SetupRequired = false
				},
				new Site { Id = "7", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 0, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "8", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 0, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "9", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 0, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "10", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 1, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "11", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 1, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "12", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 2, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "13", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 2, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "14", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 0, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "15", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 0, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "16", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 1, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "17", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 1, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) },
				new Site { Id = "18", Title = "Curam House", FullAddress = "1100 Broadway Mall, Hicksville, NY 11801", Status = 1, Notices = rnd.Next(0, 10), Cameras = rnd.Next(0, 10), Zones = rnd.Next(0, 3), Requests = rnd.Next(0, 5), Contacts = rnd.Next(0, 5) }
			};
		}

		#endregion
	}
}
