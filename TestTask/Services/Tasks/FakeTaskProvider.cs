﻿using System;
using System.Collections.Generic;
using TestTask.Models;
using TestTask.Services.Sites;
using TestTask.Services.Users;

namespace TestTask.Services.Tasks
{
	public class FakeTaskProvider : ITaskProvider
	{
		private readonly IUserProvider _userProvider;
		private readonly ISiteProvider _siteProvider;

		public FakeTaskProvider(IUserProvider userProvider,
			ISiteProvider siteProvider)
		{
			_userProvider = userProvider;
			_siteProvider = siteProvider;
		}


		#region Implementation of ITaskProvider

		public List<SiteTask> GetCurrentUserTasks(int count)
		{
			var sites = _siteProvider.GetSites(null);

			return new List<SiteTask>
			{
				new SiteTask
				{
					Id = null,
					User = _userProvider.GetRandomUser(),
					Site = sites[0],
					DateTime = DateTime.Now.AddDays(-1),
					DateTime2 = DateTime.Now,
					Title = "There are spiders on cameras",
					Message = "Please fix it as soon as possible"
				},
				new SiteTask
				{
					Id = null,
					User = _userProvider.GetRandomUser(),
					Site = sites[1],
					DateTime = DateTime.Now.AddDays(-1),
					DateTime2 = DateTime.Now,
					Title = "There are spiders on cameras",
					Message = "Please fix it as soon as possible"
				}
			};
		}

		public int GetCurrentUserTasksCount()
		{
			return 5;
		}

		#endregion
	}
}
