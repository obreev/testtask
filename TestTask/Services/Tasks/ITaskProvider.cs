﻿using System.Collections.Generic;
using TestTask.Models;

namespace TestTask.Services.Tasks
{
	public interface ITaskProvider
	{
		List<SiteTask> GetCurrentUserTasks(int count);

		int GetCurrentUserTasksCount();
	}
}
