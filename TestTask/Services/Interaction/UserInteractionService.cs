﻿using System.Threading.Tasks;
using TestTask.Services.Logging;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace TestTask.Services.Interaction
{
	public class UserInteractionService : BaseService, IUserInteractionService
	{
		private readonly IMainThread _mainThread;

		public UserInteractionService(IMainThread mainThread)
		{
			_mainThread = mainThread;
		}


		#region Implementation of IUserInteractionService

		public Task DisplayAlertAsync(string title, string message)
		{
			return DisplayAlertAsync(title, message, Localization.Common.AlertCancelText);
		}

		public Task DisplayAlertAsync(string title, string message, string cancel)
		{
			_log.Debug(this, $"{nameof(title)}:{title}; {nameof(message)}:{message}");
			if (Application.Current is App app)
			{
				return _mainThread.InvokeOnMainThreadAsync(() => app.MainPage.DisplayAlert(title, message, cancel));
			}
			return Task.CompletedTask;
		}

		public Task<bool> DisplayConfirmationAsync(string title, string message)
		{
			return DisplayConfirmationAsync(title, message, Localization.Common.ConfirmationAcceptText, Localization.Common.ConfirmationDeclineText);
		}

		public Task<bool> DisplayConfirmationAsync(string title, string message, string accept, string decline)
		{
			_log.Debug(this, $"{nameof(title)}:{title}; {nameof(message)}:{message}");
			if (Application.Current is App app)
			{
				var result = _mainThread.InvokeOnMainThreadAsync(() => app.MainPage.DisplayAlert(title, message, accept, decline));
				_log.Debug(this, $"{nameof(result)}:{result}");
				return result;
			}
			return Task.FromResult(true);
		}

		#endregion
	}
}
