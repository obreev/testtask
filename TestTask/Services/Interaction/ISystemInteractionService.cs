﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace TestTask.Services.Interaction
{
	public interface ISystemInteractionService
	{
		Task ShowSettingsAsync();

		Task OpenBrowserAsync(Uri uri);

		Task OpenBrowserAsync(string url, BrowserLaunchOptions? browserLaunchOptions = null);

		Task OpenAppAsync(string deepLinkingUrl, string browserUrl);

		Task OpenAppAsync(string appScheme, string deepLinkingUrl, string browserUrl);

		Task SendEmailAsync(string subject, string body, IEnumerable<string> recipients, IList<EmailAttachment>? attachments = null);
	}
}
