﻿using System.Threading.Tasks;

namespace TestTask.Services.Interaction
{
	public interface IUserInteractionService
	{
		Task DisplayAlertAsync(string title, string message);

		Task DisplayAlertAsync(string title, string message, string cancel);

		Task<bool> DisplayConfirmationAsync(string title, string message);

		Task<bool> DisplayConfirmationAsync(string title, string message, string accept, string decline);
	}
}
