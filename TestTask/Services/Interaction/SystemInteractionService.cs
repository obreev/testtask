﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Services.Logging;
using Xamarin.Essentials;
using Xamarin.Essentials.Interfaces;

namespace TestTask.Services.Interaction
{
	public class SystemInteractionService : BaseService, ISystemInteractionService
	{
		private readonly IAppInfo _appInfo;
		private readonly IBrowser _browser;
		private readonly IEmail _email;
		private readonly ILauncher _launcher;
		private readonly IMainThread _mainThread;
		private readonly IUserInteractionService _userInteractionService;

		public SystemInteractionService(IAppInfo appInfo,
			IBrowser browser,
			IEmail email,
			ILauncher launcher,
			IMainThread mainThread,
			IUserInteractionService userInteractionService)
		{
			_appInfo = appInfo;
			_browser = browser;
			_email = email;
			_launcher = launcher;
			_mainThread = mainThread;
			_userInteractionService = userInteractionService;
		}


		#region Implementation of ISystemInteractionService

		public Task ShowSettingsAsync()
		{
			_log.Debug(this);
			return _mainThread.InvokeOnMainThreadAsync(_appInfo.ShowSettingsUI);
		}

		public Task OpenBrowserAsync(Uri uri)
		{
			return OpenBrowserAsync(uri.ToString());
		}

		public Task OpenBrowserAsync(string url, BrowserLaunchOptions? browserLaunchOptions = null)
		{
			_log.Debug(this, url);
			return _mainThread.InvokeOnMainThreadAsync(() =>
			{
				browserLaunchOptions ??= new BrowserLaunchOptions
				{
					//PreferredControlColor = NavigationBarTextColor,
					//PreferredToolbarColor = NavigationBarBackgroundColor,
					Flags = BrowserLaunchFlags.PresentAsFormSheet
				};

				return _browser.OpenAsync(url, browserLaunchOptions);
			});
		}

		public Task OpenAppAsync(string deepLinkingUrl, string browserUrl)
		{
			return OpenAppAsync(deepLinkingUrl, deepLinkingUrl, browserUrl);
		}

		public Task OpenAppAsync(string appScheme, string deepLinkingUrl, string browserUrl)
		{
			_log.Debug(this, $"{nameof(deepLinkingUrl)}:{deepLinkingUrl}; {nameof(browserUrl)}:{browserUrl}");
			return _mainThread.InvokeOnMainThreadAsync(async () =>
			{
				var supportsUri = await _launcher.CanOpenAsync(appScheme);
				if (supportsUri)
				{
					await _launcher.OpenAsync(deepLinkingUrl);
				}
				else
				{
					await OpenBrowserAsync(browserUrl);
				}
			});
		}

		public Task SendEmailAsync(string subject, string body, IEnumerable<string> recipients, IList<EmailAttachment>? attachments = null)
		{
			_log.Debug(this, $"{nameof(subject)}:{subject}; {nameof(body)}:{body}; {nameof(recipients)}:{recipients}");
			return _mainThread.InvokeOnMainThreadAsync(async () =>
			{
				var recipientsList = recipients.ToList();
				var message = new EmailMessage
				{
					Subject = subject,
					Body = body,
					To = recipientsList,
					Attachments = attachments?.ToList()
				};

				try
				{
					await _email.ComposeAsync(message).ConfigureAwait(false);
				}
				catch (FeatureNotSupportedException)
				{
					await _userInteractionService.DisplayAlertAsync(
						Localization.Common.EmailClientNotFoundAlertTitle, 
						$"{Localization.Common.EmailClientNotFoundAlertMessage}: {string.Join(", ", recipientsList)}").ConfigureAwait(false);
				}
			});
		}

		#endregion
	}
}
