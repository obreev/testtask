﻿using Autofac;
using TestTask.Services.Logging;

namespace TestTask.Services
{
	public abstract class BaseService
	{
		protected readonly ILog _log;

		protected BaseService()
		{
			_log = IoC.Container.Resolve<ILog>();
		}
	}
}
