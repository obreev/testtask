﻿using System;
using TestTask.Models;

namespace TestTask.Services.Users
{
	public class FakeUserProvider : IUserProvider
	{
		#region Implementation of IUserProvider

		public User GetCurrentUser()
		{
			return new User
			{
				Id = Guid.Empty,
				FirstName = "Jenny",
				LastName = "Wil",
				AvatarUrl = "",
				JobTitle = "Manager",
				CompanyName = "Service Ltd.",
				Phone = "+441233453445",
				Email = "will@service.com",
				PreferedContactMethod = 1
			};
		}

		public User GetRandomUser()
		{
			return new User
			{
				Id = Guid.Empty,
				FirstName = "Jackelyne",
				LastName = "Perra",
				AvatarUrl = null,
				JobTitle = null,
				CompanyName = null,
				Phone = null,
				Email = null,
				PreferedContactMethod = 0
			};
		}

		#endregion
	}
}
