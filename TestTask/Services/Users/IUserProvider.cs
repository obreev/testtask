﻿using TestTask.Models;

namespace TestTask.Services.Users
{
	public interface IUserProvider
	{
		User GetCurrentUser();

		User GetRandomUser();
	}
}
