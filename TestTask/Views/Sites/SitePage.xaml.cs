﻿using System;
using System.Threading.Tasks;
using TestTask.Helpers.Mvvm;
using Xamarin.Forms;

namespace TestTask.Views.Sites
{
	public partial class SitePage
	{
		public SitePage()
		{
			InitializeComponent();
		}

		private double _pageHeight;

		#region Overrides of BaseContentPage<SitePageViewModel>

		protected override void OnAppearing()
		{
			PanGestureRecognizer.PanUpdated += PanGestureRecognizerOnPanUpdated;
			_pageHeight = This.Height;
			base.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			PanGestureRecognizer.PanUpdated -= PanGestureRecognizerOnPanUpdated;
			base.OnDisappearing();
		}

		#endregion

		private void PanGestureRecognizerOnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			OnPanUpdatedAsync(sender, e).SafeFireAndForget();
		}

		private double _backNavigationScrollRatio = 1.0 / 3;
		private bool _backNavigationFired;

		private async Task OnPanUpdatedAsync(object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType)
			{
				case GestureStatus.Started:
					break;

				case GestureStatus.Running:
					if (e.TotalY / _pageHeight > _backNavigationScrollRatio)
					{
						if (_backNavigationFired)
							return;
						_backNavigationFired = true;

						await ViewModel.GoBackCommand.ExecuteAsync();
					}
					await DraggableContainer.TranslateTo(0, e.TotalY, 40, Easing.Linear);
					break;

				case GestureStatus.Completed:
					await DraggableContainer.TranslateTo(0, 0, 200, Easing.CubicOut);
					break;

				case GestureStatus.Canceled:
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
