﻿<?xml version="1.0" encoding="utf-8" ?>
<base:BaseContentPage xmlns="http://xamarin.com/schemas/2014/forms"
                      xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
                      xmlns:d="http://xamarin.com/schemas/2014/forms/design"
                      xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      mc:Ignorable="d"

                      xmlns:base="clr-namespace:TestTask.Views.Base;assembly=TestTask"
                      xmlns:viewModel="clr-namespace:TestTask.ViewModels.Sites;assembly=TestTask"
                      xmlns:fa="clr-namespace:TestTask.Helpers.Fa;assembly=TestTask"
                      xmlns:sk="clr-namespace:Xamarin.Forms.Skeleton;assembly=Xamarin.Forms.Skeleton"
                      xmlns:controls="clr-namespace:TestTask.Controls;assembly=TestTask"
                      xmlns:models="clr-namespace:TestTask.Models;assembly=TestTask"
                      xmlns:converters="clr-namespace:TestTask.Converters;assembly=TestTask"

                      x:Class="TestTask.Views.Sites.SitesPage"
                      x:TypeArguments="viewModel:SitesPageViewModel"
                      x:DataType="viewModel:SitesPageViewModel"
                      x:Name="Page"

                      xmlns:sharedTransitions="clr-namespace:Plugin.SharedTransitions;assembly=Plugin.SharedTransitions"
                      sharedTransitions:SharedTransitionNavigationPage.TransitionSelectedGroup="{Binding SelectedSite.Id}"

                      Visual="Material"
                      NavigationPage.HasNavigationBar="False">

	<!-- ReSharper disable Xaml.BindingWithContextNotResolved -->
	<ContentPage.Resources>
		<ResourceDictionary>

			<OnPlatform x:Key="NavigationBarHeight" x:TypeArguments="x:Double">
				<On Platform="iOS" Value="44" />
				<On Platform="Android" Value="48" />
			</OnPlatform>

			<OnPlatform x:Key="NavigationBarHalfHeight" x:TypeArguments="x:Int32">
				<On Platform="iOS" Value="22" />
				<On Platform="Android" Value="24" />
			</OnPlatform>

			<Style x:Key="NavigationBarImageButtonStyle" TargetType="ImageButton">
		        <Setter Property="WidthRequest" Value="{StaticResource NavigationBarHeight}" />
		        <Setter Property="HeightRequest" Value="{StaticResource NavigationBarHeight}" />
		        <Setter Property="CornerRadius" Value="{StaticResource NavigationBarHalfHeight}" />
	            <Setter Property="BackgroundColor" Value="{StaticResource PrimaryBackgroundColor}" />
	            <Setter Property="VerticalOptions" Value="Center" />
		        <Setter Property="Padding" Value="10" />
			</Style>

			<Style x:Key="SitesListTitleStyle" TargetType="Label">
				<Setter Property="FontSize" Value="Medium" />
				<Setter Property="TextColor" Value="{StaticResource PrimaryColor}" />
			</Style>

			<Style x:Key="SitesListDescriptionStyle" TargetType="Label">
				<Setter Property="FontSize" Value="Small" />
				<Setter Property="TextColor" Value="{StaticResource SecondaryColor}" />
			</Style>

			<converters:IsBusyBackgroundColorConverter x:Key="IsBusyBackgroundColorConverter" />

			<DataTemplate x:Key="SiteItemTemplate" x:DataType="models:Site">
				<Grid RowDefinitions="*, Auto"
				      BackgroundColor="{StaticResource PrimaryBackgroundColor}">
					<Grid.GestureRecognizers>
						<TapGestureRecognizer Command="{Binding Source={x:Reference Page}, Path=BindingContext.GoToSiteCommand}"
						                      CommandParameter="{Binding .}"
						                      NumberOfTapsRequired="1" />
					</Grid.GestureRecognizers>
					<Grid ColumnDefinitions="*, Auto"
					      Padding="12, 8, 12, 8">
						<StackLayout Orientation="Vertical">
							<StackLayout Orientation="Horizontal">

								<Frame VerticalOptions="Center"
								       Margin="0, 0, 8, 0"
								       sk:Skeleton.IsBusy="{Binding Source={x:Reference Page}, Path=BindingContext.IsBusy}"
								       BackgroundColor="{Binding Source={x:Reference Page}, Path=BindingContext.IsBusy,
											Converter={StaticResource IsBusyBackgroundColorConverter},
											ConverterParameter={StaticResource PrimaryColor}}">
									<Label FontFamily="FaLight"
									       Text="{x:Static fa:FaLight.Shield}"
									       Style="{StaticResource SitesListTitleStyle}" />
								</Frame>

								<Label Text="{Binding Title}"
								       LineBreakMode="TailTruncation"
								       sharedTransitions:Transition.Name="SiteTitle"
								       sharedTransitions:Transition.Group="{Binding Id}"
								       Style="{StaticResource SitesListTitleStyle}"
								       sk:Skeleton.IsBusy="{Binding Source={x:Reference Page}, Path=BindingContext.IsBusy}"
								       BackgroundColor="{Binding Source={x:Reference Page}, Path=BindingContext.IsBusy,
												Converter={StaticResource IsBusyBackgroundColorConverter},
												ConverterParameter={StaticResource PrimaryColor}}" />
								
							</StackLayout>

							<Label Text="{Binding FullAddress}"
							       LineBreakMode="TailTruncation"
							       sharedTransitions:Transition.Name="SiteFullAddress"
							       sharedTransitions:Transition.Group="{Binding Id}"
							       Style="{StaticResource SitesListDescriptionStyle}"
								   Margin="0, 2, 0, 0"
							       sk:Skeleton.IsBusy="{Binding Source={x:Reference Page}, Path=BindingContext.IsBusy}"
							       BackgroundColor="{Binding Source={x:Reference Page}, Path=BindingContext.IsBusy,
											Converter={StaticResource IsBusyBackgroundColorConverter},
											ConverterParameter={StaticResource SecondaryColor}}" />
							
						</StackLayout>
						<Label Grid.Column="1"
						       VerticalOptions="Center"
						       FontFamily="FaLight"
						       Text="{x:Static fa:FaLight.ChevronRight}"
						       Margin="8, 0, 0, 0" />
					</Grid>

					<BoxView Grid.Row="1"
					         HeightRequest="1"
					         BackgroundColor="{StaticResource ListSeparatorColor}" />
				</Grid>
			</DataTemplate>

		</ResourceDictionary>
	</ContentPage.Resources>

	<ContentPage.Content>
        <AbsoluteLayout BackgroundColor="{StaticResource PrimaryBackgroundColor}">

	        <Grid AbsoluteLayout.LayoutFlags="All"
	              AbsoluteLayout.LayoutBounds="0, 0, 1, 1"
	              RowDefinitions="Auto, Auto, Auto, *">

				<!-- custom NavigationBar -->
		        <Grid ColumnDefinitions="Auto, Auto, *, Auto, Auto"
		              HeightRequest="{StaticResource NavigationBarHeight}"
		              Margin="0, 16, 0, 16">

			        <!-- back arrow -->
					<ImageButton Style="{StaticResource NavigationBarImageButtonStyle}"
					             Command="{Binding GoBackCommand}">
						<ImageButton.Source>
							<FontImageSource FontFamily="FaLight"
							                 Glyph="{x:Static fa:FaLight.ChevronLeft}"
							                 Color="{StaticResource PrimaryColor}" />
						</ImageButton.Source>
					</ImageButton>
                    <Grid Grid.Column="1" WidthRequest="{StaticResource NavigationBarHeight}" />

			        <!-- title -->
                    <Label Grid.Column="2"
                           Text="Sites"
                           StyleClass="LargeLabelStyle"
                           HorizontalOptions="CenterAndExpand"
                           VerticalOptions="Center" />

			        <!-- map button -->
					<ImageButton Grid.Column="3"
					             Style="{StaticResource NavigationBarImageButtonStyle}">
                        <ImageButton.Source>
                            <FontImageSource FontFamily="FaLight"
                                             Glyph="{x:Static fa:FaLight.Map}"
                                             Color="{StaticResource PrimaryColor}" />
                        </ImageButton.Source>
					</ImageButton>

			        <!-- settings button -->
                    <ImageButton Grid.Column="4"
                                 Style="{StaticResource NavigationBarImageButtonStyle}">
	                    <ImageButton.Source>
		                    <FontImageSource FontFamily="FaLight"
		                                     Glyph="{x:Static fa:FaLight.SlidersV}"
		                                     Color="{StaticResource PrimaryColor}" />
	                    </ImageButton.Source>
                    </ImageButton>

				</Grid>

				<!-- search -->
				<controls:SearchView Grid.Row="1"
				                     Margin="12, 0, 12, 12" />

				<BoxView Grid.Row="2"
				         HeightRequest="1"
				         BackgroundColor="{StaticResource ListSeparatorColor}" />

				<!-- sites list -->
				<CollectionView Grid.Row="3"
				                BackgroundColor="{StaticResource SecondaryBackgroundColor}"
				                ItemsSource="{Binding SitesCollection}"
								SelectedItem="{Binding SelectedSite}"
				                ItemTemplate="{StaticResource SiteItemTemplate}"
								SelectionMode="Single"
				                sk:Skeleton.IsParent="True"
				                sk:Skeleton.IsBusy="{Binding IsBusy}"
				                sk:Skeleton.Animation="{sk:DefaultAnimation Fade}" />
			</Grid>

			<!-- add new -->
			<controls:AddButton AbsoluteLayout.LayoutFlags="PositionProportional"
			                    AbsoluteLayout.LayoutBounds="1, 1, -1, -1"
			                    Text="Add new"
			                    Margin="0, 0, 24, 24" />
			
        </AbsoluteLayout>
	</ContentPage.Content>

</base:BaseContentPage>