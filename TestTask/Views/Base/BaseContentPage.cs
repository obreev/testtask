﻿using System;
using System.Threading.Tasks;
using Autofac;
using TestTask.Helpers.Mvvm;
using TestTask.Services.Logging;
using TestTask.ViewModels.Base;
using Xamarin.Forms;

namespace TestTask.Views.Base
{
	public class BaseContentPage<T> : BaseContentPage, IBaseContentPage<T>
		where T : class, IBasePageViewModel
	{
		protected readonly ILog _log;

		protected BaseContentPage()
		{
			_log = IoC.Container.Resolve<ILog>();
		}


		#region Implementation of IBaseContentPage<T>

		public virtual T ViewModel => BindingContext as T;

		public void SetBindingContext(T bindingContext)
		{
			BindingContext = bindingContext;
		}

		#endregion


		protected override void OnAppearing()
		{
			base.OnAppearing();
			OnAppearingAsync().SafeFireAndForget(OnAppearingException);
		}

		private async Task OnAppearingAsync()
		{
			_log.Debug(this);

			if (ViewModel != null)
			{
				await ViewModel.OnAppearingAsync().ConfigureAwait(false);
			}
		}

		private void OnAppearingException(Exception e)
		{
			_log.Fatal(this, e);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			OnDisappearingAsync().SafeFireAndForget(OnDisappearingException);
		}

		private async Task OnDisappearingAsync()
		{
			_log.Debug(this);

			if (ViewModel != null)
			{
				await ViewModel.OnDisappearingAsync().ConfigureAwait(false);
			}
		}

		private void OnDisappearingException(Exception e)
		{
			_log.Fatal(this, e);
		}
	}

	public class BaseContentPage : ContentPage
	{
		public static readonly BindableProperty HasSystemPaddingProperty = BindableProperty.Create(nameof(HasSystemPadding),
			typeof(bool), typeof(BaseContentPage), true);

		public static readonly BindableProperty SystemPaddingProperty = BindableProperty.Create(nameof(SystemPadding),
			typeof(Thickness), typeof(BaseContentPage), default(Thickness), BindingMode.OneWayToSource);

		public static readonly BindableProperty SystemPaddingSidesProperty = BindableProperty.Create(nameof(SystemPaddingSides),
			typeof(PaddingSide), typeof(BaseContentPage), PaddingSide.All);

		public static readonly BindableProperty StatusBarStyleProperty = BindableProperty.Create(nameof(StatusBarStyle),
			typeof(StatusBarStyle), typeof(BaseContentPage), StatusBarStyle.DarkText);

		public bool HasSystemPadding
		{
			get => (bool)GetValue(HasSystemPaddingProperty);
			set => SetValue(HasSystemPaddingProperty, value);
		}

		public Thickness SystemPadding
		{
			get => (Thickness)GetValue(SystemPaddingProperty);
			internal set => SetValue(SystemPaddingProperty, value);
		}

		public PaddingSide SystemPaddingSides
		{
			get => (PaddingSide)GetValue(SystemPaddingSidesProperty);
			set => SetValue(SystemPaddingSidesProperty, value);
		}

		public StatusBarStyle StatusBarStyle
		{
			get => (StatusBarStyle)GetValue(StatusBarStyleProperty);
			set => SetValue(StatusBarStyleProperty, value);
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);

			switch (propertyName)
			{
				case nameof(HasSystemPadding):
				case nameof(SystemPadding):
				case nameof(SystemPaddingSides):
					ForceLayout();
					break;
			}
		}

		protected override void LayoutChildren(double x, double y, double width, double height)
		{
			if (HasSystemPadding)
			{
				var systemPadding = SystemPadding;
				var systemPaddingSide = SystemPaddingSides;

				var left = 0d;
				var top = 0d;
				var right = 0d;
				var bottom = 0d;

				if (systemPaddingSide.HasFlag(PaddingSide.Left))
					left = systemPadding.Left;
				if (systemPaddingSide.HasFlag(PaddingSide.Top))
					top = systemPadding.Top;
				if (systemPaddingSide.HasFlag(PaddingSide.Right))
					right = systemPadding.Right;
				if (systemPaddingSide.HasFlag(PaddingSide.Bottom))
					bottom = systemPadding.Bottom;

				x += left;
				y += top;
				width -= left + right;
				height -= top + bottom;
			}

			OnLayoutChildren();

			base.LayoutChildren(x, y, width, height);
		}

		protected virtual void OnLayoutChildren()
		{
		}
	}
}
