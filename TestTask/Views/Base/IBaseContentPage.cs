﻿using TestTask.ViewModels.Base;

namespace TestTask.Views.Base
{
	public interface IBaseContentPage<T>
		where T : IBasePageViewModel
	{
		T ViewModel { get; }

		void SetBindingContext(T bindingContext);
	}
}
