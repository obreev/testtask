﻿using System;
using System.Threading.Tasks;
using Autofac;
using Rg.Plugins.Popup.Pages;
using TestTask.Helpers.Mvvm;
using TestTask.Services.Logging;
using TestTask.ViewModels.Base;

namespace TestTask.Views.Base
{
	public class BasePopupContentPage<T> : PopupPage, IBaseContentPage<T>
		where T : class, IBasePageViewModel
	{
		protected readonly ILog _log;

		protected BasePopupContentPage()
		{
			_log = IoC.Container.Resolve<ILog>();
		}


		#region Implementation of IBaseContentPage<T>

		public virtual T ViewModel => BindingContext as T;

		public void SetBindingContext(T bindingContext)
		{
			BindingContext = bindingContext;
		}

		#endregion


		protected override void OnAppearing()
		{
			base.OnAppearing();
			OnAppearingAsync().SafeFireAndForget(OnAppearingException);
		}

		private async Task OnAppearingAsync()
		{
			_log.Debug(this);

			if (ViewModel != null)
			{
				await ViewModel.OnAppearingAsync().ConfigureAwait(false);
			}
		}

		private void OnAppearingException(Exception e)
		{
			_log.Fatal(this, e);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			OnDisappearingAsync().SafeFireAndForget(OnDisappearingException);
		}

		private async Task OnDisappearingAsync()
		{
			_log.Debug(this);

			if (ViewModel != null)
			{
				await ViewModel.OnDisappearingAsync().ConfigureAwait(false);
			}
		}

		private void OnDisappearingException(Exception e)
		{
			_log.Fatal(this, e);
		}
	}
}
