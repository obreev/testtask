﻿
namespace TestTask.Views.Base
{
	public enum StatusBarStyle
	{
		Default,
		DarkText,
		WhiteText,
		Hidden
	}
}
