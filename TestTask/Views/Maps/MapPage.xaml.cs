﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TestTask.Helpers;
using TestTask.Helpers.Mvvm;
using TestTask.Models;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Map = Xamarin.Forms.GoogleMaps.Map;

namespace TestTask.Views.Maps
{
	public partial class MapPage
	{
		public MapPage()
		{
			InitializeComponent();
		}

		#region Overrides of BaseContentPage

		protected override void OnLayoutChildren()
		{
			NavigationBar.Padding = new Thickness(0, SystemPadding.Top, 0, 0);
			ControlsLayer.Padding = new Thickness(0, 0, 0, SystemPadding.Bottom);
		}

		#endregion

		#region Overrides of BaseContentPage<MapPageViewModel>

		protected override void OnAppearing()
		{
			if (Map == null)
			{
				CreateMap();
			}
			base.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			if (Map != null)
			{
				Map.SelectedPinChanged -= MapOnSelectedPinChanged;
			}

			base.OnDisappearing();
		}

		#endregion

		private Map Map { get; set; }

		private List<Site> Sites { get; set; }

		private CameraUpdate StartupCameraUpdate { get; set; }


		private void CreateMap()
		{
			Sites = ViewModel.GetSites();

			StartupCameraUpdate = MapHelper.GetCameraUpdate(Sites) ?? MapHelper.GetDefaultCameraUpdate();

			Map = new Map
			{
				InitialCameraUpdate = StartupCameraUpdate
			};

			Map.SelectedPinChanged += MapOnSelectedPinChanged;

			ProcessPins();
			ProcessMapStyle();

			MapLayer.Children.Add(Map);
			MapLayer.ForceLayout();
		}

		private bool _pinsInitialized;
		private bool _blockPinChangedHandler;

		private void MapOnSelectedPinChanged(object sender, SelectedPinChangedEventArgs e)
		{
			if (_blockPinChangedHandler)
				return;

			if (!_pinsInitialized)
			{
				_pinsInitialized = true;
				return;
			}

			ProcessCameraUpdateOnPin(e.SelectedPin, true).SafeFireAndForget();
		}

		private async Task ProcessCameraUpdateOnPin(Pin pin, bool scrollToPin)
		{
			_blockOnCurrentItemHandler = true;

			if (pin == null)
			{
				//await Map.AnimateCamera(StartupCameraUpdate);
			}
			else
			{
				var pinCameraUpdate = MapHelper.GetCameraUpdate(pin.Position, Map.CameraPosition.Zoom);
				await Map.AnimateCamera(pinCameraUpdate);

				if (scrollToPin)
				{
					CarouselView.ScrollTo(pin.BindingContext as Site, position: ScrollToPosition.Center);
				}
			}

			_blockOnCurrentItemHandler = false;
		}


		private void ProcessPins()
		{
			var pins = MapHelper.GetPins(Sites);
			if (pins == null)
				return;

			foreach (var pin in pins)
			{
				Map.Pins.Add(pin);
			}
		}

		private void ProcessMapStyle()
		{
			var mapStyle = GetMapStyle();
			if (mapStyle != null)
			{
				Map.MapStyle = MapStyle.FromJson(mapStyle);
			}
		}

		private string GetMapStyle()
		{
			try
			{
				var assembly = typeof(MapPage).GetTypeInfo().Assembly;
				var stream = assembly.GetManifestResourceStream("TestTask.Resources.Maps.mapStyle.js");
				if (stream == null)
					return null;

				using var reader = new StreamReader(stream);
				return reader.ReadToEnd();
			}
			catch (Exception)
			{
				return null;
			}
		}

		private bool _currentItemInitialized;
		private bool _blockOnCurrentItemHandler;

		private void CarouselView_OnCurrentItemChanged(object sender, CurrentItemChangedEventArgs e)
		{
			if (_blockOnCurrentItemHandler)
				return;

			if (!_currentItemInitialized)
			{
				_currentItemInitialized = true;
				return;
			}

			_blockPinChangedHandler = true;

			if (!(e.CurrentItem is Site currentSite))
				return;

			var currentPin = Map.Pins.FirstOrDefault(pin =>
			{
				var site = pin.BindingContext as Site;
				return currentSite.Id.Equals(site?.Id);
			});

			if (currentPin == null)
				return;

			//Map.SelectedPin = currentPin;
			ProcessCameraUpdateOnPin(currentPin, false).SafeFireAndForget();

			_blockPinChangedHandler = false;
		}
	}
}
