﻿using System.Threading.Tasks;
using Autofac;
using TestTask.Helpers.Mvvm;
using TestTask.Services.Routing;
using TestTask.ViewModels.Home;

namespace TestTask.Views
{
	public partial class MainPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

		private bool _initialized;

		#region Overrides of Page

		protected override void OnAppearing()
		{
			if (_initialized)
			{
				return;
			}
			_initialized = true;

			OnAppearingAsync().SafeFireAndForget();
			base.OnAppearing();
		}

		private async Task OnAppearingAsync()
		{
			await Task.Delay(100);
			var router = IoC.Container.Resolve<IRouter>();
			await router.OpenNavigationPageAsync<HomePageViewModel>();
		}

		protected override bool OnBackButtonPressed()
		{
			return true;
		}

		#endregion
	}
}
