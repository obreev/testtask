﻿
namespace TestTask.Views.Home
{
	public partial class HomePage
	{
		public HomePage()
		{
			InitializeComponent();
		}

		#region Overrides of Page

		protected override bool OnBackButtonPressed()
		{
			return true;
		}

		#endregion
	}
}
