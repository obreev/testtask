﻿using Xamarin.Forms;

namespace TestTask.Views.Profile
{
	public partial class ProfilePage
	{
		public ProfilePage()
		{
			InitializeComponent();
		}

		#region Overrides of BaseContentPage

		protected override void OnLayoutChildren()
		{
			NavigationBar.Padding = new Thickness(0, SystemPadding.Top, 0, 0);
		}

		#endregion
	}
}
