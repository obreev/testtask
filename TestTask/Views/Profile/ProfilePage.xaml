﻿<?xml version="1.0" encoding="utf-8" ?>
<base:BaseContentPage xmlns="http://xamarin.com/schemas/2014/forms"
                      xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
                      xmlns:d="http://xamarin.com/schemas/2014/forms/design"
                      xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      mc:Ignorable="d"

                      xmlns:base="clr-namespace:TestTask.Views.Base;assembly=TestTask"
                      xmlns:viewModel="clr-namespace:TestTask.ViewModels.Profile;assembly=TestTask"
                      xmlns:fa="clr-namespace:TestTask.Helpers.Fa;assembly=TestTask"
                      xmlns:sv="clr-namespace:AiForms.Renderers;assembly=SettingsView"
                      xmlns:controls="clr-namespace:TestTask.Controls;assembly=TestTask"
                      xmlns:validation="clr-namespace:TestTask.Converters.Validation;assembly=TestTask"
                      xmlns:behaviors="clr-namespace:TestTask.Behaviors.Validation;assembly=TestTask"
                      xmlns:converters="clr-namespace:TestTask.Converters;assembly=TestTask"

                      x:Class="TestTask.Views.Profile.ProfilePage"
                      x:TypeArguments="viewModel:ProfilePageViewModel"
                      x:DataType="viewModel:ProfilePageViewModel"

                      xmlns:sharedTransitions="clr-namespace:Plugin.SharedTransitions;assembly=Plugin.SharedTransitions"

                      Visual="Material"
                      NavigationPage.HasNavigationBar="False"
                      SystemPaddingSides="Left, Right, Bottom"
                      StatusBarStyle="WhiteText">

	<ContentPage.Resources>
		<ResourceDictionary>

			<validation:FirstValidationErrorConverter x:Key="FirstValidationErrorConverter" />
			<converters:InvertedBoolConverter x:Key="InvertedBoolConverter" />

			<Style x:Key="ErrorTextStyle" TargetType="Label">
				<Setter Property="TextColor" Value="{StaticResource ProfileErrorColor}" />
				<Setter Property="FontSize" Value="11" />
				<Setter Property="Margin" Value="16, 0, 16, 0" />
			</Style>

			<Style TargetType="Entry">
				<Setter Property="BackgroundColor" Value="{StaticResource PrimaryBackgroundColor}" />
				<Setter Property="PlaceholderColor" Value="Gray" />
				<Setter Property="TextColor" Value="{StaticResource PrimaryColor}" />
			</Style>

			<Style x:Key="NavigationBarButtonStyle" TargetType="Button">
				<Setter Property="BackgroundColor" Value="{StaticResource ProfileHeaderColor}" />
				<Setter Property="TextColor" Value="{StaticResource PrimaryBackgroundColor}" />
				<Setter Property="FontSize" Value="Medium" />
				<Setter Property="BorderColor" Value="Transparent" />
				<Setter Property="BorderWidth" Value="0" />
				<Setter Property="CornerRadius" Value="12" />
				<Setter Property="Visual" Value="Default" />
				<Setter Property="Padding" Value="10" />
			</Style>

		</ResourceDictionary>
	</ContentPage.Resources>

	<ContentPage.Content>
        <controls:CustomScrollView BackgroundColor="{StaticResource ProfileHeaderColor}">
	        <Grid RowDefinitions="Auto, Auto, Auto, Auto, Auto">

		        <!-- navigation bar -->
		        <Grid x:Name="NavigationBar"
		              ColumnDefinitions="Auto, *, Auto"
		              BackgroundColor="{StaticResource ProfileHeaderColor}"
		              HeightRequest="{StaticResource NavigationBarHeight}">

			        <Button Text="Cancel"
			                Command="{Binding CancelCommand}"
			                IsVisible="{Binding UserDataChanged}"
			                Style="{StaticResource NavigationBarButtonStyle}" />
			        <ImageButton Grid.Column="2"
			                     Command="{Binding GoBackCommand}"
			                     IsVisible="{Binding UserDataChanged, Converter={StaticResource InvertedBoolConverter}}"
			                     Style="{StaticResource NavigationBarImageButtonStyle}"
			                     BackgroundColor="{StaticResource ProfileHeaderColor}">
				        <ImageButton.Source>
					        <FontImageSource FontFamily="FaLight"
					                         Glyph="{x:Static fa:FaLight.Times}"
					                         Color="{StaticResource PrimaryBackgroundColor}" />
				        </ImageButton.Source>
			        </ImageButton>
					<Button Grid.Column="2"
					        Text="  Save"
					        Command="{Binding SaveCommand}"
					        IsVisible="{Binding UserDataChanged}"
					        Style="{StaticResource NavigationBarButtonStyle}" />
		        </Grid>
            
		        <!-- avatar -->
		        <Grid Grid.Row="1"
		              RowDefinitions="Auto, Auto, Auto"
		              BackgroundColor="{StaticResource ProfileHeaderColor}"
		              Padding="16, 16, 16, 32">

			        <Grid HorizontalOptions="Center"
			              VerticalOptions="Center"
			              Margin="16"
			              sharedTransitions:Transition.Name="CurrentUserAvatarContainer">

						<Grid WidthRequest="120"
						      HeightRequest="120"
						      HorizontalOptions="Center"
						      VerticalOptions="Center">
							<controls:AvatarView Text="{Binding FullNameInitials}"
							                     Source="{Binding AvatarUrl}"
							                     Size="120" />
						</Grid>

						<Frame BackgroundColor="{StaticResource PrimaryBackgroundColor}"
							   CornerRadius="10"
							   BorderColor="{StaticResource PrimaryColor}"
							   HorizontalOptions="End"
						       VerticalOptions="Start"
							   Margin="0, 8, 8, 0">
							<Label FontSize="Medium"
							       FontFamily="FaLight"
							       HorizontalOptions="End"
							       VerticalOptions="Start"
							       Text="{x:Static fa:FaLight.Times}"
							       TextColor="{StaticResource PrimaryColor}"
							       Margin="4, 0, 4, 0" />
						</Frame>

			        </Grid>

			        <Label Grid.Row="1"
			               Text="{Binding FullName}"
						   TextColor="{StaticResource PrimaryBackgroundColor}"
						   FontSize="36"
			               HorizontalOptions="Center"
			               sharedTransitions:Transition.Name="CurrentUserFullName" />
					<Label Grid.Row="2"
					       FontSize="Small"
					       TextColor="{StaticResource PrimaryBackgroundColor}"
					       HorizontalOptions="Center">
						<Label.FormattedText>
							<FormattedString>
								<Span Text="{Binding JobTitle}" />
								<Span Text=" of " />
								<Span Text="{Binding CompanyName}" />
							</FormattedString>
						</Label.FormattedText>
					</Label>

		        </Grid>

		        <!-- edit form -->
				<StackLayout Grid.Row="2"
				             BackgroundColor="{StaticResource PrimaryBackgroundColor}"
				             Padding="0, 16, 0, 0">

					<!-- First Name -->
					<Entry Text="{Binding FirstName.Value}" 
					       Placeholder="First Name *">
						<Entry.Behaviors>
							<behaviors:EntryLineValidationBehaviour IsValid="{Binding FirstName.IsValid}"/>
						</Entry.Behaviors>
					</Entry>

					<Label Text="{Binding FirstName.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding FirstName.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /First Name -->

					<!-- Last Name -->
					<Entry Text="{Binding LastName.Value}"
					       Placeholder="Last Name *">
						<Entry.Behaviors>
							<behaviors:EntryLineValidationBehaviour IsValid="{Binding LastName.IsValid}"/>
						</Entry.Behaviors>
					</Entry>

					<Label Text="{Binding LastName.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding LastName.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /Last Name -->

					<!-- PreferedContactMethod -->
					<Picker Title="Prefered method to contact *"
							ItemsSource="{Binding PreferedContactItems}"
							SelectedItem="{Binding SelectedPreferedContact}"
							ItemDisplayBinding="{Binding Description}"
					        TitleColor="Gray"
					        TextColor="{StaticResource PrimaryColor}"
					        BackgroundColor="{StaticResource PrimaryBackgroundColor}">
					</Picker>

					<Label Text="{Binding PreferedContactMethod.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding PreferedContactMethod.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /PreferedContactMethod -->

					<!-- Phone number -->
					<Entry Text="{Binding Phone.Value}"
					       Placeholder="Phone number *">
						<Entry.Behaviors>
							<behaviors:EntryLineValidationBehaviour IsValid="{Binding Phone.IsValid}"/>
						</Entry.Behaviors>
					</Entry>

					<Label Text="{Binding Phone.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding Phone.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /Phone number -->

					<!-- Email -->
					<Entry Text="{Binding Email.Value}"
					       Placeholder="Email *">
						<Entry.Behaviors>
							<behaviors:EntryLineValidationBehaviour IsValid="{Binding Email.IsValid}"/>
						</Entry.Behaviors>
					</Entry>

					<Label Text="{Binding Email.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding Email.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /Email -->

					<!-- Job Title -->
					<Entry Text="{Binding JobTitle.Value}"
					       Placeholder="Job title *">
						<Entry.Behaviors>
							<behaviors:EntryLineValidationBehaviour IsValid="{Binding JobTitle.IsValid}"/>
						</Entry.Behaviors>
					</Entry>

					<Label Text="{Binding JobTitle.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding JobTitle.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /Job Title -->

					<!-- Company Name -->
					<Entry Text="{Binding CompanyName.Value}"
						   TextColor="{StaticResource SecondaryColor}"
					       Placeholder="Company name *"
					       IsEnabled="False">
						<Entry.Behaviors>
							<behaviors:EntryLineValidationBehaviour IsValid="{Binding CompanyName.IsValid}"/>
						</Entry.Behaviors>
					</Entry>

					<Label Text="{Binding CompanyName.Errors, Converter={StaticResource FirstValidationErrorConverter}}"
					       IsVisible="{Binding CompanyName.IsValid, Converter={StaticResource InvertedBoolConverter}}"
					       Style="{StaticResource ErrorTextStyle}" />
					<!-- /Company Name -->

				</StackLayout>

		        <!-- settings view -->
				<sv:SettingsView Grid.Row="3"
				                 x:Name="SettingsView" 
				                 HasUnevenRows="True"
				                 ShowArrowIndicatorForAndroid="True"
				                 HeightRequest="{Binding VisibleContentHeight, Source={x:Reference SettingsView}}"
				                 RowHeight="48">
					<sv:Section Title="INFORMATION"
					            HeaderHeight="80">
						<sv:CommandCell Title="About"
						                Command="{Binding OpenAboutCommand}" />
						<sv:CommandCell Title="FAQ"
						                Command="{Binding OpenFaqCommand}" />
						<sv:CommandCell Title="Terms and Conditions"
						                Command="{Binding OpenTermsAndConditionsCommand}" />
						<sv:CommandCell Title="Privacy Policy"
						                Command="{Binding OpenPrivacyPolicyCommand}" />
						<sv:CommandCell Title="Contact us"
						                Command="{Binding OpenContactUsCommand}" />
					</sv:Section>
					<sv:Section>
						<sv:CommandCell Title="Logout"
										Command="{Binding LogoutCommand}"
										TitleColor="{StaticResource BlueAccentColor}"
						                HideArrowIndicator="True" />
					</sv:Section>
				</sv:SettingsView>

		        <!-- info -->
				<StackLayout Grid.Row="4"
				             Orientation="Vertical"
				             BackgroundColor="{StaticResource SecondaryBackgroundColorAlt}"
							 Padding="16, 32, 16, 16">
					<Frame WidthRequest="160"
					       HeightRequest="60"
						   HorizontalOptions="Center"
						   VerticalOptions="Center"
					       BackgroundColor="{StaticResource ListSeparatorColor}"
						   CornerRadius="0"
					       Margin="8, 8, 8, 8" />
					<Label Text="{Binding Version}"
					       StyleClass="MicroLabelStyle"
					       HorizontalOptions="Center" />
				</StackLayout>
	        </Grid>
        </controls:CustomScrollView>
	</ContentPage.Content>

</base:BaseContentPage>