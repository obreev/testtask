﻿using Autofac;
using TestTask.ViewModels.Profile;

namespace TestTask.Views.Profile
{
	public partial class HelloCurrentUserView
	{
		public HelloCurrentUserView()
		{
			InitializeComponent();
			BindingContext = IoC.Container.Resolve<HelloCurrentUserViewModel>();
		}
	}
}
