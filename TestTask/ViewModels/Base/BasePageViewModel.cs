﻿using System.Threading.Tasks;
using Autofac;
using TestTask.Helpers.Mvvm;
using TestTask.Services.Logging;

namespace TestTask.ViewModels.Base
{
	public abstract class BasePageViewModel : ObservableObject, IBasePageViewModel
	{
		protected readonly ILog _log;

		protected BasePageViewModel()
		{
			_log = IoC.Container.Resolve<ILog>();
		}


		#region Implementation of IBasePageViewModel

		public virtual Task InitializeAsync(params object[] parameters)
		{
			_log.Debug(this);
			return Task.CompletedTask;
		}

		public virtual Task OnAppearingAsync()
		{
			_log.Debug(this);
			return Task.CompletedTask;
		}

		public virtual Task OnDisappearingAsync()
		{
			_log.Debug(this);
			return Task.CompletedTask;
		}


		private string _title;
		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}

		private string _subtitle;
		public string Subtitle
		{
			get => _subtitle;
			set => SetProperty(ref _subtitle, value);
		}

		private bool _isBusy;
		public bool IsBusy
		{
			get => _isBusy;
			set => SetProperty(ref _isBusy, value);
		}

		#endregion
	}
}
