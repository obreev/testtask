﻿using System.Threading.Tasks;

namespace TestTask.ViewModels.Base
{
	public interface IBasePageViewModel
	{
		Task InitializeAsync(params object[] parameters);


		Task OnAppearingAsync();

		Task OnDisappearingAsync();


		string Title { get; set; }

		string Subtitle { get; set; }

		bool IsBusy { get; set; }
	}
}
