﻿using System.Threading.Tasks;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Services.Routing;
using TestTask.ViewModels.Base;

namespace TestTask.ViewModels.Home
{
	public class AddNewPageViewModel : BasePageViewModel
	{
		private readonly IRouter _router;

		public AddNewPageViewModel(IRouter router)
		{
			_router = router;
		}


		#region Commands

		public AsyncCommand GoBackCommand => new AsyncCommand(GoBackAsync);

		private async Task GoBackAsync()
		{
			await _router.ClosePopupAsync(true);
		}

		#endregion
	}
}
