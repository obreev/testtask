﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Models;
using TestTask.Services.Routing;
using TestTask.Services.Sites;
using TestTask.Services.Tasks;
using TestTask.ViewModels.Base;
using TestTask.ViewModels.Maps;
using TestTask.ViewModels.Sites;

namespace TestTask.ViewModels.Home
{
	public class HomePageViewModel : BasePageViewModel
	{
		private readonly IRouter _router;
		private readonly ITaskProvider _taskProvider;
		private readonly ISiteProvider _siteProvider;

		public HomePageViewModel(IRouter router,
			ITaskProvider taskProvider,
			ISiteProvider siteProvider)
		{
			_router = router;
			_taskProvider = taskProvider;
			_siteProvider = siteProvider;
		}


		#region Overrides of BasePageViewModel

		public override async Task InitializeAsync(params object[] parameters)
		{
			var sites = _siteProvider.GetSites(null);

			IsBusy = true;

			SitesCollection = new ObservableCollection<Site>(sites.Take(3));

			var siteTasks = _taskProvider.GetCurrentUserTasks(2);
			foreach (var siteTask in siteTasks)
			{
				siteTask.User.FirstName = null;
				siteTask.User.LastName = null;
				siteTask.User.AvatarUrl = "";
			}
			SiteTasksCollection = new ObservableCollection<SiteTask>(siteTasks);

			//simulate real query
			await Task.Delay(2000);

			siteTasks = _taskProvider.GetCurrentUserTasks(2);
			SiteTasksCollection = new ObservableCollection<SiteTask>(siteTasks);

			SiteTasksCount = _taskProvider.GetCurrentUserTasksCount();

			IsBusy = false;
		}

		#endregion


		#region Properties

		private ObservableCollection<Site> _sitesCollection;
		public ObservableCollection<Site> SitesCollection
		{
			get => _sitesCollection;
			set
			{
				_sitesCollection = value;
				OnPropertyChanged();
			}
		}

		private Site _selectedSite;
		public Site SelectedSite
		{
			get => _selectedSite;
			set
			{
				_selectedSite = value;
				OnPropertyChanged();
			}
		}

		private ObservableCollection<SiteTask> _siteTasksCollection;
		public ObservableCollection<SiteTask> SiteTasksCollection
		{
			get => _siteTasksCollection;
			set
			{
				_siteTasksCollection = value;
				OnPropertyChanged();
			}
		}

		private int _siteTasksCount;
		public int SiteTasksCount
		{
			get => _siteTasksCount;
			set
			{
				_siteTasksCount = value;
				OnPropertyChanged();
			}
		}

		#endregion


		#region Commands

		public AsyncCommand GoToMapCommand => new AsyncCommand(GoToMapAsync);

		private async Task GoToMapAsync()
		{
			await _router.OpenNavigationPageAsync<MapPageViewModel>();
		}

		public AsyncCommand GoToSitesCommand => new AsyncCommand(GoToSitesAsync);

		private async Task GoToSitesAsync()
		{
			await _router.OpenNavigationPageAsync<SitesPageViewModel>();
		}


		public AsyncCommand AddNewCommand => new AsyncCommand(AddNewAsync);

		private async Task AddNewAsync()
		{
			await _router.OpenPopupPageAsync<AddNewPageViewModel>(true);
		}


		public AsyncCommand<Site> GoToSiteCommand => new AsyncCommand<Site>(GoToSiteAsync);

		private async Task GoToSiteAsync(Site site)
		{
			if (site == null)
				return;

			SelectedSite = site;

			await _router.OpenNavigationPageAsync<SitePageViewModel>(false, site);
		}

		#endregion
	}
}
