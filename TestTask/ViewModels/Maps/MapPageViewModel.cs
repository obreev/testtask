﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Models;
using TestTask.Services.Sites;
using TestTask.ViewModels.Base;

namespace TestTask.ViewModels.Maps
{
	public class MapPageViewModel : BasePageViewModel
	{
		private readonly ISiteProvider _siteProvider;

		public MapPageViewModel(ISiteProvider siteProvider)
		{
			_siteProvider = siteProvider;
		}

		#region Overrides of BasePageViewModel

		public override Task InitializeAsync(params object[] parameters)
		{
			var sites = _siteProvider.GetSites(null).Take(5);
			SitesCollection = new ObservableCollection<Site>(sites);

			return Task.CompletedTask;
		}

		#endregion

		public List<Site> GetSites()
		{
			return SitesCollection.ToList();
		}


		#region Properties

		private ObservableCollection<Site> _sitesCollection;
		public ObservableCollection<Site> SitesCollection
		{
			get => _sitesCollection;
			set
			{
				_sitesCollection = value;
				OnPropertyChanged();
			}
		}

		#endregion

		private Site _selectedSite;
		public Site SelectedSite
		{
			get => _selectedSite;
			set
			{
				_selectedSite = value;
				OnPropertyChanged();
			}
		}


		#region Commands

		public AsyncCommand GoToSearchCommand => new AsyncCommand(GoToSearchAsync);

		private Task GoToSearchAsync()
		{
			return Task.CompletedTask;
		}

		public AsyncCommand GoToFilterCommand => new AsyncCommand(GoToFilterAsync);

		private Task GoToFilterAsync()
		{
			return Task.CompletedTask;
		}

		#endregion
	}
}
