﻿using TestTask.Helpers.Mvvm;
using Xamarin.Forms;

namespace TestTask.ViewModels.Sites
{
	public class SiteTileViewModel : ObservableObject
	{
		#region Properties

		public string Title { get; set; }

		public string Glyph { get; set; }


		private string _count;
		public string Count
		{
			get => _count;
			set
			{
				_count = value;
				OnPropertyChanged();
			}
		}

		private string _countDescription;
		public string CountDescription
		{
			get => _countDescription;
			set
			{
				_countDescription = value;
				OnPropertyChanged();
			}
		}

		private Color _color;
		public Color Color
		{
			get => _color;
			set
			{
				_color = value;
				OnPropertyChanged();
			}
		}

		#endregion


		#region Commands


		#endregion
	}
}
