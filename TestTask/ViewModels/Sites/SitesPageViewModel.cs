﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Models;
using TestTask.Services.Routing;
using TestTask.Services.Sites;
using TestTask.ViewModels.Base;

namespace TestTask.ViewModels.Sites
{
	public class SitesPageViewModel : BasePageViewModel
	{
		private readonly ISiteProvider _siteProvider;
		private readonly IRouter _router;

		public SitesPageViewModel(ISiteProvider siteProvider,
			IRouter router)
		{
			_siteProvider = siteProvider;
			_router = router;
		}

		#region Overrides of BasePageViewModel

		public override async Task InitializeAsync(params object[] parameters)
		{
			var sites = _siteProvider.GetSites(null);

			IsBusy = true;

			SitesCollection = new ObservableCollection<Site>(sites.Take(5));

			//simulate real query
			await Task.Delay(2000);

			SitesCollection = new ObservableCollection<Site>(sites);

			IsBusy = false;
		}

		#endregion


		#region Properties

		private ObservableCollection<Site> _sitesCollection;
		public ObservableCollection<Site> SitesCollection
		{
			get => _sitesCollection;
			set
			{
				_sitesCollection = value;
				OnPropertyChanged();
			}
		}

		private Site _selectedSite;
		public Site SelectedSite
		{
			get => _selectedSite;
			set
			{
				_selectedSite = value;
				OnPropertyChanged();
			}
		}

		#endregion


		#region Commands

		public AsyncCommand<Site> GoToSiteCommand => new AsyncCommand<Site>(GoToSiteAsync);

		private async Task GoToSiteAsync(Site site)
		{
			if (site == null)
				return;

			SelectedSite = site;

			await _router.OpenNavigationPageAsync<SitePageViewModel>(false, site);
		}

		public AsyncCommand GoBackCommand => new AsyncCommand(GoBackAsync);

		private async Task GoBackAsync()
		{
			await _router.CloseNavigationPageAsync();
		}

		#endregion
	}
}
