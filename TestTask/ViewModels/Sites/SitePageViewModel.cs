﻿using System.Linq;
using System.Threading.Tasks;
using Autofac;
using TestTask.Helpers.Fa;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Models;
using TestTask.Services.Routing;
using TestTask.Services.Users;
using TestTask.ViewModels.Base;
using TestTask.ViewModels.Profile;
using Xamarin.Forms;

namespace TestTask.ViewModels.Sites
{
	public class SitePageViewModel : BasePageViewModel
	{
		private readonly IUserProvider _userProvider;
		private readonly IRouter _router;

		public SitePageViewModel(IUserProvider userProvider,
			IRouter router)
		{
			_userProvider = userProvider;
			_router = router;
		}

		#region Overrides of BasePageViewModel

		public override async Task InitializeAsync(params object[] parameters)
		{
			await base.InitializeAsync(parameters);
			CurrentSite = parameters.Any() ? parameters.GetValue(0) as Site : null;

			//как-то флаг должен вычисляться из CurrentSite
			SetupIsNotCompleted = true;

			CurrentUser = _userProvider.GetCurrentUser();

			PrepareTiles(CurrentSite);
		}

		#endregion


		#region Properties

		private Site _currentSite;
		public Site CurrentSite
		{
			get => _currentSite;
			set
			{
				_currentSite = value;
				OnPropertyChanged();
			}
		}

		private User _currentUser;
		public User CurrentUser
		{
			get => _currentUser;
			set
			{
				_currentUser = value;
				OnPropertyChanged();
			}
		}

		private bool _setupIsNotCOmpleted;
		public bool SetupIsNotCompleted
		{
			get => _setupIsNotCOmpleted;
			set
			{
				_setupIsNotCOmpleted = value;
				OnPropertyChanged();
			}
		}

		public SiteTileViewModel ArmingTile { get; set; }

		public SiteTileViewModel VisitorsTile { get; set; }

		public SiteTileViewModel ServicesTile { get; set; }

		public SiteTileViewModel CamerasAndZonesTile { get; set; }

		public SiteTileViewModel ContactsTile { get; set; }

		public SiteTileViewModel SettingsTile { get; set; }

		#endregion


		#region Commands

		public AsyncCommand GoBackCommand => new AsyncCommand(GoBackAsync);

		private async Task GoBackAsync()
		{
			await _router.CloseNavigationPageAsync();
		}

		public AsyncCommand GoToProfileCommand => new AsyncCommand(GoToProfileAsync);

		private async Task GoToProfileAsync()
		{
			await _router.OpenNavigationPageAsync<ProfilePageViewModel>();
		}

		#endregion


		private void PrepareTiles(Site currentSite)
		{
			//Хардкод :(
			//Но пока ничего не понятно с логикой цветовой индикации
			if (currentSite == null)
				return;

			ArmingTile = IoC.Container.Resolve<SiteTileViewModel>();
			ArmingTile.Title = "Arming";
			ArmingTile.Count = CurrentSite.Notices.ToString();
			ArmingTile.CountDescription = "notices";
			ArmingTile.Glyph = FaLight.Shield;
			ArmingTile.Color = SiteTileNormalColor;

			VisitorsTile = IoC.Container.Resolve<SiteTileViewModel>();
			VisitorsTile.Title = "Visitors";
			VisitorsTile.Count = "No";
			VisitorsTile.CountDescription = "visitors";
			VisitorsTile.Glyph = FaLight.Truck;
			VisitorsTile.Color = SiteTileNormalColor;

			ServicesTile = IoC.Container.Resolve<SiteTileViewModel>();
			ServicesTile.Title = "Services";
			ServicesTile.Count = CurrentSite.Requests.ToString();
			ServicesTile.CountDescription = "requests";
			ServicesTile.Glyph = FaLight.Tools;
			ServicesTile.Color = SiteTileWarningColor;

			CamerasAndZonesTile = IoC.Container.Resolve<SiteTileViewModel>();
			CamerasAndZonesTile.Title = "Cameras and Zones";
			CamerasAndZonesTile.Count = $"{CurrentSite.Cameras} / {CurrentSite.Zones}";
			CamerasAndZonesTile.CountDescription = "notices";
			CamerasAndZonesTile.Glyph = FaLight.Camera;
			CamerasAndZonesTile.Color = SiteTileCriticalColor;

			ContactsTile = IoC.Container.Resolve<SiteTileViewModel>();
			ContactsTile.Title = "Contacts";
			ContactsTile.Count = CurrentSite.Contacts.ToString();
			ContactsTile.CountDescription = "contacts";
			ContactsTile.Glyph = FaLight.AddressBook;
			ContactsTile.Color = SiteTileNormalColor;

			SettingsTile = IoC.Container.Resolve<SiteTileViewModel>();
			SettingsTile.Title = "Settings";
			SettingsTile.Glyph = FaLight.Cog;
			SettingsTile.Color = SiteTileNormalColor;
		}

		// TODO: get from resources

		private static Color SiteTileEmptyColor { get; } = Color.FromHex("#9A254A5D");

		private static Color SiteTileNormalColor { get; } = Color.FromHex("#009FDF");

		private static Color SiteTileWarningColor { get; } = Color.FromHex("#FF9500");

		private static Color SiteTileCriticalColor { get; } = Color.FromHex("#FF3B30");
	}
}
