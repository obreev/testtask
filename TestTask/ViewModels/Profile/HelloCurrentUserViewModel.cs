﻿using System.Threading.Tasks;
using TestTask.Helpers.Mvvm;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Models;
using TestTask.Services.Routing;
using TestTask.Services.Users;

namespace TestTask.ViewModels.Profile
{
	public class HelloCurrentUserViewModel : ObservableObject
	{
		private readonly IRouter _router;

		public HelloCurrentUserViewModel(IRouter router,
			IUserProvider userProvider)
		{
			_router = router;

			HelloText = "Good morning,";
			CurrentUser = userProvider.GetCurrentUser();
		}


		#region Properties

		private string _helloText;
		public string HelloText
		{
			get => _helloText;
			set
			{
				_helloText = value;
				OnPropertyChanged();
			}
		}

		private User _currentUser;
		public User CurrentUser
		{
			get => _currentUser;
			set
			{
				_currentUser = value;
				OnPropertyChanged();
			}
		}

		#endregion


		#region Commands

		public AsyncCommand GoToProfileCommand => new AsyncCommand(GoToProfileAsync);

		private async Task GoToProfileAsync()
		{
			await _router.OpenNavigationPageAsync<ProfilePageViewModel>();
		}

		#endregion
	}
}
