﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Helpers.Mvvm.Commands;
using TestTask.Models;
using TestTask.Services.Interaction;
using TestTask.Services.Routing;
using TestTask.Services.Users;
using TestTask.Validation;
using TestTask.Validation.Rules;
using TestTask.ViewModels.Base;
using Xamarin.Essentials.Interfaces;

namespace TestTask.ViewModels.Profile
{
	public class ProfilePageViewModel : BasePageViewModel
	{
		private readonly IRouter _router;
		private readonly IUserProvider _userProvider;
		private readonly IVersionTracking _versionTracking;
		private readonly ISystemInteractionService _systemInteractionService;

		public ProfilePageViewModel(IRouter router,
			IUserProvider userProvider,
			IVersionTracking versionTracking,
			ISystemInteractionService systemInteractionService)
		{
			_router = router;
			_userProvider = userProvider;
			_versionTracking = versionTracking;
			_systemInteractionService = systemInteractionService;
		}


		#region Overrides of BasePageViewModel

		public override Task InitializeAsync(params object[] parameters)
		{
			var currentUser = _userProvider.GetCurrentUser();
			if (currentUser == null)
				return Task.CompletedTask;

			OriginalUser = currentUser;

			InitializeUserForm(currentUser);

			SelectedPreferedContact = PreferedContactItems.FirstOrDefault(contact => contact.Code == currentUser.PreferedContactMethod);

			AddValidationRules();
			AreFieldsValid();

			Version = _versionTracking.CurrentVersion;

			FirstName.PropertyChanged += ValidatableObjectOnPropertyChanged;
			LastName.PropertyChanged += ValidatableObjectOnPropertyChanged;
			JobTitle.PropertyChanged += ValidatableObjectOnPropertyChanged;
			Phone.PropertyChanged += ValidatableObjectOnPropertyChanged;
			Email.PropertyChanged += ValidatableObjectOnPropertyChanged;

			return base.InitializeAsync(parameters);
		}

		private void InitializeUserForm(User currentUser)
		{
			FirstName.Value = new string(currentUser.FirstName);
			LastName.Value = new string(currentUser.LastName);
			AvatarUrl = new string(currentUser.AvatarUrl);
			JobTitle.Value = new string(currentUser.JobTitle);
			CompanyName.Value = new string(currentUser.CompanyName);
			Phone.Value = new string(currentUser.Phone);
			Email.Value = new string(currentUser.Email);
			PreferedContactMethod.Value = currentUser.PreferedContactMethod;
		}

		private void ValidatableObjectOnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (!e.PropertyName.Equals("Value"))
				return;

			OnPropertyChanged(nameof(FullNameInitials));
			OnPropertyChanged(nameof(FullName));

			AreFieldsValid();
			CheckUserForChanges();
		}

		#endregion


		#region Properties

		public User OriginalUser { get; set; }

		public ValidatableObject<string> FirstName { get; set; } = new ValidatableObject<string>();

		public ValidatableObject<string> LastName { get; set; } = new ValidatableObject<string>();

		public string FullNameInitials => $"{FirstName?.Value?.Substring(0, 1)} {LastName?.Value?.Substring(0, 1)}";

		public string FullName => $"{FirstName} {LastName}";

		private string _avatarUrl;
		public string AvatarUrl
		{
			get => _avatarUrl;
			set
			{
				_avatarUrl = value;
				OnPropertyChanged();
			}
		}

		public ValidatableObject<string> JobTitle { get; set; } = new ValidatableObject<string>();

		public ValidatableObject<string> CompanyName { get; set; } = new ValidatableObject<string>();

		public ValidatableObject<string> Phone { get; set; } = new ValidatableObject<string>();

		public ValidatableObject<string> Email { get; set; } = new ValidatableObject<string>();

		public ValidatableObject<int> PreferedContactMethod { get; set; } = new ValidatableObject<int>();

		public List<PreferedContact> PreferedContactItems { get; set; } = new List<PreferedContact>
		{
			new PreferedContact { Code = 0, Description = "by phone"},
			new PreferedContact { Code = 1, Description = "by email"}
		};

		private PreferedContact _selectedPreferedContact;
		public PreferedContact SelectedPreferedContact
		{
			get => _selectedPreferedContact;
			set 
			{
				_selectedPreferedContact = value;
				OnPropertyChanged();
				CheckUserForChanges();
			}
		}

		private string _version;
		public string Version
		{
			get => _version;
			set
			{
				_version = value;
				OnPropertyChanged();
			}
		}

		private bool _userDataChanged;
		public bool UserDataChanged
		{
			get => _userDataChanged;
			set
			{
				_userDataChanged = value;
				OnPropertyChanged();
			}
		}

		#endregion


		#region Validation

		private void AddValidationRules()
		{
			FirstName.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "First Name required" });
			LastName.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Last Name required" });
			JobTitle.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Jon Title required" });
			Phone.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Phone required" });
			Email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Email required" });
			Email.Validations.Add(new IsValidEmailRule<string> { ValidationMessage = "Invalid Email" });
		}

		private bool AreFieldsValid()
		{
			var isFirstNameValid = FirstName.Validate();
			var isLastNameValid = LastName.Validate();
			var isJobTitleValid = JobTitle.Validate();
			var isPhoneValid = Phone.Validate();
			var isEmailValid = Email.Validate();

			return isFirstNameValid
			       && isLastNameValid
			       && isJobTitleValid
				   && isPhoneValid
			       && isEmailValid;
		}

		#endregion


		#region Commands

		public AsyncCommand GoBackCommand => new AsyncCommand(GoBackAsync);

		private async Task GoBackAsync()
		{
			await _router.CloseNavigationPageAsync();
		}

		public AsyncCommand CancelCommand => new AsyncCommand(CancelAsync);

		private Task CancelAsync()
		{
			InitializeUserForm(OriginalUser);
			return Task.CompletedTask;
		}

		public AsyncCommand SaveCommand => new AsyncCommand(SaveAsync);

		private Task SaveAsync()
		{
			OriginalUser.FirstName = new string(FirstName.Value);
			OriginalUser.LastName = new string(LastName.Value);
			OriginalUser.AvatarUrl = new string(AvatarUrl);
			OriginalUser.JobTitle = new string(JobTitle.Value);
			OriginalUser.Phone = new string(Phone.Value);
			OriginalUser.Email = new string(Email.Value);
			OriginalUser.PreferedContactMethod = PreferedContactMethod.Value;

			InitializeUserForm(OriginalUser);
			CheckUserForChanges();

			return Task.CompletedTask;
		}

		public AsyncCommand OpenAboutCommand => new AsyncCommand(OpenAboutAsync);

		private Task OpenAboutAsync()
		{
			return _systemInteractionService.OpenBrowserAsync(AppSettings.AboutUrl);
		}

		public AsyncCommand OpenFaqCommand => new AsyncCommand(OpenFaqAsync);

		private Task OpenFaqAsync()
		{
			return _systemInteractionService.OpenBrowserAsync(AppSettings.FaqUrl);
		}

		public AsyncCommand OpenTermsAndConditionsCommand => new AsyncCommand(OpenTermsAndConditionsAsync);

		private Task OpenTermsAndConditionsAsync()
		{
			return _systemInteractionService.OpenBrowserAsync(AppSettings.TermsAndConditionsUrl);
		}

		public AsyncCommand OpenPrivacyPolicyCommand => new AsyncCommand(OpenPrivacyPolicyAsync);

		private Task OpenPrivacyPolicyAsync()
		{
			return _systemInteractionService.OpenBrowserAsync(AppSettings.PrivacyPolicyUrl);
		}

		public AsyncCommand OpenContactUsCommand => new AsyncCommand(OpenContactUsAsync);

		private Task OpenContactUsAsync()
		{
			return _systemInteractionService.OpenBrowserAsync(AppSettings.ContuctUsUrl);
		}

		public AsyncCommand LogoutCommand => new AsyncCommand(LogoutAsync);

		private Task LogoutAsync()
		{
			return Task.CompletedTask;
		}

		#endregion


		private void CheckUserForChanges()
		{
			if (FirstName.Value.Equals(OriginalUser.FirstName)
			    && LastName.Value.Equals(OriginalUser.LastName)
			    && AvatarUrl.Equals(OriginalUser.AvatarUrl)
			    && JobTitle.Value.Equals(OriginalUser.JobTitle)
			    && CompanyName.Value.Equals(OriginalUser.CompanyName)
			    && Phone.Value.Equals(OriginalUser.Phone)
			    && Email.Value.Equals(OriginalUser.Email)
			    && SelectedPreferedContact.Code == OriginalUser.PreferedContactMethod)
			{
				UserDataChanged = false;
			}
			else
			{
				UserDataChanged = true;
			}
		}
	}

	public class PreferedContact
	{
		public int Code { get; set; }

		public string Description { get; set; }
	}
}
