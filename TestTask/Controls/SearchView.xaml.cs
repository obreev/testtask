﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTask.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchView
	{
		public SearchView()
		{
			InitializeComponent();
		}

		private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
		{
			SearchEntry.Text = "";
		}

		private void InputView_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			var searchText = e.NewTextValue;
			ClearButton.IsVisible = !string.IsNullOrEmpty(searchText);
		}
	}
}