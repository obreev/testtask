﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTask.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddButton
	{
		public static readonly BindableProperty TextProperty = BindableProperty.Create(
			nameof(Text), typeof(string), typeof(AddButton));

		public static readonly BindableProperty CommandProperty = BindableProperty.Create(
			nameof(Command), typeof(ICommand), typeof(AddButton));

		public string Text
		{
			get => (string) GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}

		public ICommand Command
		{
			get => (ICommand) GetValue(CommandProperty);
			set => SetValue(CommandProperty, value);
		}

		public AddButton()
		{
			InitializeComponent();
		}

		#region Overrides of Element

		protected override void OnPropertyChanged(string propertyName = null)
		{
			switch (propertyName)
			{
				case nameof(Text):
					ButtonLabel.Text = Text;
					break;

				case nameof(Command):
					TapGestureRecognizer.Command = Command;
					break;
			}
		}

		#endregion
	}
}
