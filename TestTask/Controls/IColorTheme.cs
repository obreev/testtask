﻿using Xamarin.Forms;

namespace TestTask.Controls
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>see XamarinCommunityToolkit</remarks>
	public interface IColorTheme
	{
		Color GetForegroundColor(string text);

		Color GetBackgroundColor(string text);
	}
}
