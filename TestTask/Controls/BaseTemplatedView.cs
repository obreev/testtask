﻿using System.ComponentModel;
using Xamarin.Forms;

namespace TestTask.Controls
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TControl"></typeparam>
	/// <remarks>see XamarinCommunityToolkit</remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	public abstract class BaseTemplatedView<TControl>
		: TemplatedView where TControl : View, new()
	{
		protected TControl Control { get; private set; }

		protected BaseTemplatedView() => ControlTemplate = new ControlTemplate(typeof(TControl));

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			Control.BindingContext = BindingContext;
		}

		protected override void OnChildAdded(Element child)
		{
			if (Control == null && child is TControl content)
			{
				Control = content;
				OnControlInitialized(Control);
			}
			base.OnChildAdded(child);
		}

		protected abstract void OnControlInitialized(TControl control);
	}
}
