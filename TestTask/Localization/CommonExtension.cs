﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTask.Localization
{
	[ContentProperty(nameof(Text))]
	public class CommonExtension : IMarkupExtension
	{
		private readonly CultureInfo _cultureInfo;

		private const string ResourceId = "TestTask.Localization.Common";

		private static readonly Lazy<ResourceManager> ResourcesManagerLazy = new Lazy<ResourceManager>(() => 
			new ResourceManager(ResourceId, typeof(CommonExtension).GetTypeInfo().Assembly));

		public CommonExtension()
		{
			_cultureInfo = CultureInfo.CurrentCulture;
		}

		public string Text { get; set; }

		public object ProvideValue(IServiceProvider serviceProvider)
		{
			if (Text == null)
			{
				return null;
			}
			
			var localizedString = ResourcesManagerLazy.Value.GetString(Text, _cultureInfo) ?? Text;
			if (localizedString != null)
			{
				return localizedString;
			}
				
			Debug.WriteLine($"Key '{Text}' was not found in resources '{ResourceId}' for culture '{_cultureInfo.Name}'.", nameof(Text));

			// hack: returns the key, which gets displayed to the user
			return Text;
		}
	}
}
