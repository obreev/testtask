﻿using Autofac;
using Plugin.SharedTransitions;
using TestTask.Services.Logging;
using TestTask.Views;
using Xamarin.Forms;

namespace TestTask
{
	public partial class App
	{
		private readonly ILog _log;

		private SharedTransitionNavigationPage RootPage { get; }

		public App(ContainerBuilder containerBuilder)
		{
			Device.SetFlags(new [] { "Shapes_Experimental", "Brush_Experimental" });

			InitializeComponent();

			DoRegisterContainer(containerBuilder);

			RootPage = new SharedTransitionNavigationPage(new MainPage());
			containerBuilder.RegisterInstance(RootPage.Navigation).As<INavigation>().SingleInstance();
			
			IoC.Container = containerBuilder.Build();

			_log = IoC.Container.Resolve<ILog>();

			MainPage = RootPage;
		}

		protected override void OnStart()
		{
			_log.Debug("App started");
		}

		protected override void OnSleep()
		{
			_log.Debug("App backgrounded");
		}

		protected override void OnResume()
		{
			_log.Debug("App resumed");
		}
	}
}
