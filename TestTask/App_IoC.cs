﻿using Autofac;
using Rg.Plugins.Popup.Pages;
using TestTask.Services.Interaction;
using TestTask.Services.Logging;
using TestTask.Services.Maps;
using TestTask.Services.Routing;
using TestTask.Services.Sites;
using TestTask.Services.Tasks;
using TestTask.Services.Users;
using TestTask.ViewModels.Home;
using TestTask.ViewModels.Maps;
using TestTask.ViewModels.Profile;
using TestTask.ViewModels.Sites;
using TestTask.Views.Home;
using TestTask.Views.Maps;
using TestTask.Views.Profile;
using TestTask.Views.Sites;
using Xamarin.Essentials.Implementation;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace TestTask
{
	public partial class App
	{
		private void DoRegisterContainer(ContainerBuilder containerBuilder)
		{
			DoRegisterEssentials(containerBuilder);
			DoRegisterServices(containerBuilder);
			DoRegisterViewModels(containerBuilder);
		}

		private void DoRegisterEssentials(ContainerBuilder containerBuilder)
		{
			containerBuilder.RegisterType<AppInfoImplementation>().As<IAppInfo>().SingleInstance();
			containerBuilder.RegisterType<BrowserImplementation>().As<IBrowser>().SingleInstance();
			containerBuilder.RegisterType<DeviceInfoImplementation>().As<IDeviceInfo>().SingleInstance();
			containerBuilder.RegisterType<EmailImplementation>().As<IEmail>().SingleInstance();
			containerBuilder.RegisterType<LauncherImplementation>().As<ILauncher>().SingleInstance();
			containerBuilder.RegisterType<MainThreadImplementation>().As<IMainThread>().SingleInstance();
			containerBuilder.RegisterType<VersionTrackingImplementation>().As<IVersionTracking>().SingleInstance();
		}

		private void DoRegisterServices(ContainerBuilder containerBuilder)
		{
			containerBuilder.RegisterType<Log>().As<ILog>().SingleInstance();
			containerBuilder.RegisterType<Router>().As<IRouter>().SingleInstance();

			containerBuilder.RegisterType<UserInteractionService>().As<IUserInteractionService>().SingleInstance();
			containerBuilder.RegisterType<SystemInteractionService>().As<ISystemInteractionService>().SingleInstance();

			containerBuilder.RegisterType<FakeUserProvider>().As<IUserProvider>().SingleInstance();
			containerBuilder.RegisterType<FakeSiteProvider>().As<ISiteProvider>().SingleInstance();
			containerBuilder.RegisterType<FakeTaskProvider>().As<ITaskProvider>().SingleInstance();
			containerBuilder.RegisterType<FakeGeoProvider>().As<IGeoProvider>().SingleInstance();
		}

		private void DoRegisterViewModels(ContainerBuilder containerBuilder)
		{
			containerBuilder.RegisterType<HomePage>().Keyed<Page>(typeof(HomePageViewModel));
			containerBuilder.RegisterType<HomePageViewModel>().AsSelf();
			containerBuilder.RegisterType<AddNewPage>().Keyed<PopupPage>(typeof(AddNewPageViewModel));
			containerBuilder.RegisterType<AddNewPageViewModel>().AsSelf();

			containerBuilder.RegisterType<ProfilePage>().Keyed<Page>(typeof(ProfilePageViewModel));
			containerBuilder.RegisterType<ProfilePageViewModel>().AsSelf();
			containerBuilder.RegisterType<HelloCurrentUserViewModel>();

			containerBuilder.RegisterType<SitesPage>().Keyed<Page>(typeof(SitesPageViewModel));
			containerBuilder.RegisterType<SitesPageViewModel>().AsSelf();
			containerBuilder.RegisterType<SitePage>().Keyed<Page>(typeof(SitePageViewModel));
			containerBuilder.RegisterType<SitePageViewModel>().AsSelf();

			containerBuilder.RegisterType<MapPage>().Keyed<Page>(typeof(MapPageViewModel));
			containerBuilder.RegisterType<MapPageViewModel>().AsSelf();

			containerBuilder.RegisterType<SiteTileViewModel>();
		}
	}
}
