﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Xamarin.Forms;

namespace TestTask.Behaviors
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public abstract class BaseBehavior<TView> : Behavior<TView> where TView : View
	{
		// ReSharper disable once StaticMemberInGenericType
		private static readonly MethodInfo GetContextMethod = typeof(BindableObject).GetRuntimeMethods()?.FirstOrDefault(m => m.Name == "GetContext");

		// ReSharper disable once StaticMemberInGenericType
		private static readonly FieldInfo BindingField = GetContextMethod?.ReturnType.GetRuntimeField("Binding");

		private BindingBase _defaultBindingContextBinding;

		protected TView AssociatedObject { get; private set; }

		protected virtual void OnViewPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}

		protected override void OnAttachedTo(TView bindable)
		{
			base.OnAttachedTo(bindable);
			AssociatedObject = bindable;
			bindable.PropertyChanged += OnViewPropertyChanged;

			if (!IsBound(BindingContextProperty))
			{
				_defaultBindingContextBinding = new Binding
				{
					Path = BindingContextProperty.PropertyName,
					Source = bindable
				};
				SetBinding(BindingContextProperty, _defaultBindingContextBinding);
			}
		}

		protected override void OnDetachingFrom(TView bindable)
		{
			base.OnDetachingFrom(bindable);

			if (_defaultBindingContextBinding != null)
			{
				RemoveBinding(BindingContextProperty);
				_defaultBindingContextBinding = null;
			}

			bindable.PropertyChanged -= OnViewPropertyChanged;
			AssociatedObject = null;
		}

		protected bool IsBound(BindableProperty property, BindingBase defaultBinding = null)
		{
			var context = GetContextMethod?.Invoke(this, new object[] { property });
			return context != null
			       && BindingField?.GetValue(context) is BindingBase binding
			       && binding != defaultBinding;
		}
	}
}
