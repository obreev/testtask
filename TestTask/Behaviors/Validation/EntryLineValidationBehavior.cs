﻿using Xamarin.Forms;

namespace TestTask.Behaviors.Validation
{
	public class EntryLineValidationBehaviour : BaseBehavior<Entry>
	{
		public static readonly BindableProperty IsValidProperty = BindableProperty.Create(nameof(IsValid),
			typeof(bool), typeof(EntryLineValidationBehaviour), true, BindingMode.Default, null, (bindable, oldValue, newValue) => OnIsValidChanged(bindable, newValue));
		
		public bool IsValid
		{
			get => (bool )GetValue(IsValidProperty);
			set => SetValue(IsValidProperty, value);
		}

		private static void OnIsValidChanged(BindableObject bindable, object newValue)
		{
			if (bindable is EntryLineValidationBehaviour isValidBehavior &&
			    newValue is bool isValid)
			{
				//todo: not in style system!
				isValidBehavior.AssociatedObject.PlaceholderColor = isValid ? Color.Default : Color.Red;
			}
		}
	}
}
