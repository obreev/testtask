﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestTask.Behaviors
{
    /// <summary>
    /// see href: https://gist.github.com/JimmyPun610/6d04f3ac59c0f1b1f5520d50f6d9714f
    /// </summary>
	public class SwipeDownToClosePopupPageBehavior : BaseBehavior<View>
	{
		public static readonly BindableProperty ClosingEdgeProperty = BindableProperty.Create(nameof(ClosingEdge), 
			typeof(double), typeof(SwipeDownToClosePopupPageBehavior), Convert.ToDouble(100), BindingMode.TwoWay, propertyChanged: ClosingEdgePropertyChanged);

		private static void ClosingEdgePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SwipeDownToClosePopupPageBehavior)bindable;
			if (newValue != null)
			{
				control.ClosingEdge = Convert.ToDouble(newValue);
			}
		}

        public static readonly BindableProperty ClosingTimeInMsProperty = BindableProperty.Create(nameof(ClosingTimeInMs),
			typeof(long), typeof(SwipeDownToClosePopupPageBehavior), Convert.ToInt64(500), BindingMode.TwoWay, propertyChanged: ClosingTimeInMsPropertyChanged);

        private static void ClosingTimeInMsPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
	        var control = (SwipeDownToClosePopupPageBehavior)bindable;
	        if (newValue != null)
	        {
		        control.ClosingTimeInMs = Convert.ToInt64(newValue);
	        }
        }

        public static readonly BindableProperty CloseCommandProperty = BindableProperty.Create(
	        nameof(CloseCommand), typeof(ICommand), typeof(SwipeDownToClosePopupPageBehavior));

        /// <summary>
        /// The height from the bottom that will trigger close page 
        /// </summary>
        public double ClosingEdge
		{
			get => (double) GetValue(ClosingEdgeProperty);
			set => SetValue(ClosingEdgeProperty, Convert.ToDouble(value));
		}

        /// <summary>
        /// Scroll time less than this value will trigger close page
        /// </summary>
        public long ClosingTimeInMs
        {
	        get => (long) GetValue(ClosingTimeInMsProperty);
	        set => SetValue(ClosingTimeInMsProperty, Convert.ToInt64(value));
        }

        public ICommand CloseCommand
        {
	        get => (ICommand)GetValue(CloseCommandProperty);
	        set => SetValue(CloseCommandProperty, value);
        }


        private PanGestureRecognizer PanGestureRecognizer { get; }

        private DateTimeOffset? StartPanDownTime { get; set; }

        private DateTimeOffset? EndPanDownTime { get; set; }

        private double TotalY { get; set; }

        private bool ReachedEdge { get; set; }


        public SwipeDownToClosePopupPageBehavior()
        {
	        PanGestureRecognizer = new PanGestureRecognizer();
        }


        protected override void OnAttachedTo(View v)
        {
            PanGestureRecognizer.PanUpdated += Pan_PanUpdated;
            v.GestureRecognizers.Add(PanGestureRecognizer);
            base.OnAttachedTo(v);
        }

        protected override void OnDetachingFrom(View v)
        {
            PanGestureRecognizer.PanUpdated -= Pan_PanUpdated;
            v.GestureRecognizers.Remove(PanGestureRecognizer);
            base.OnDetachingFrom(v);
        }

        private void Pan_PanUpdated(object sender, PanUpdatedEventArgs e)
        {
            var v = sender as View;
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    StartPanDownTime = DateTime.Now;
                    break;

                case GestureStatus.Running:
                    TotalY = e.TotalY;
                    if (TotalY > 0)
                    {
                        if (Device.RuntimePlatform == Device.Android)
                        {
	                        if (v != null)
	                        {
		                        v.TranslateTo(0, TotalY + v.TranslationY, 20, Easing.Linear);
		                        //Too close to edge?
		                        ReachedEdge = TotalY + v.TranslationY > v.Height - ClosingEdge;
	                        }
                        }

                        else
                        {
	                        if (v != null)
	                        {
		                        v.TranslateTo(0, TotalY, 20, Easing.Linear);
		                        //Too close to edge?
		                        ReachedEdge = TotalY > v.Height - ClosingEdge;
                            }
                        }
                    }
                    break;

                case GestureStatus.Completed:
                    EndPanDownTime = DateTimeOffset.Now;

                    // ReSharper disable once PossibleInvalidOperationException
                    if (EndPanDownTime.Value.ToUnixTimeMilliseconds() - StartPanDownTime.Value.ToUnixTimeMilliseconds() < ClosingTimeInMs
	                    && TotalY > 0 || ReachedEdge)
                    {
	                    //Swipe too fast
	                    CloseCommand?.Execute(null);
                    }
                    else
                    {
                        v.TranslateTo(0, 0, 20, Easing.Linear);
                    }
                    break;
                case GestureStatus.Canceled:
	                break;
                default:
	                throw new ArgumentOutOfRangeException();
            }

            if (e.StatusType != GestureStatus.Completed && e.StatusType != GestureStatus.Canceled)
	            return;

            StartPanDownTime = null;
            EndPanDownTime = null;
        }
    }
}
