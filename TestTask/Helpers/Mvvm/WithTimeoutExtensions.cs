﻿using System;
using System.Threading.Tasks;

/*
 * James Montemagno
 * https://github.com/jamesmontemagno/mvvm-helpers
 */

namespace TestTask.Helpers.Mvvm
{
	/// <summary>
	/// Task extensions
	/// </summary>
	public static class WithTimeoutExtensions
	{
		/// <summary>
		/// Task extension to add a timeout.
		/// </summary>
		/// <returns>The task with timeout.</returns>
		/// <param name="task">Task.</param>
		/// <param name="timeoutInMilliseconds">Timeout duration in Milliseconds.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static async Task<T> WithTimeout<T>(this Task<T> task, int timeoutInMilliseconds)
		{
			var retTask = await Task.WhenAny(task, Task.Delay(timeoutInMilliseconds))
				.ConfigureAwait(false);

#pragma warning disable CS8603 // Possible null reference return.
			return retTask is Task<T> ? task.Result : default;
#pragma warning restore CS8603 // Possible null reference return.
		}

		/// <summary>
		/// Task extension to add a timeout.
		/// </summary>
		/// <returns>The task with timeout.</returns>
		/// <param name="task">Task.</param>
		/// <param name="timeout">Timeout Duration.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static Task<T> WithTimeout<T>(this Task<T> task, TimeSpan timeout) =>
			WithTimeout(task, (int)timeout.TotalMilliseconds);
	}
}
