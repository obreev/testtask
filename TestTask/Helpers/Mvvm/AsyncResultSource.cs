﻿using System.Threading.Tasks;

namespace TestTask.Helpers.Mvvm
{
	public class AsyncResultSource<TResult>
	{
		private readonly TaskCompletionSource<TResult> _tcs;

		public AsyncResultSource()
		{
			_tcs = new TaskCompletionSource<TResult>();
			ReturnDefaultIfCancel = true;
		}

		public bool ReturnDefaultIfCancel { get; set; }

		public Task<TResult> GetResultAsync()
		{
			return _tcs.Task;
		}

		public void SetResult(TResult result)
		{
			_tcs.TrySetResult(result);
		}

		public void SetCanceled()
		{
			if (ReturnDefaultIfCancel)
			{
				_tcs.TrySetResult(default);
			}
			else
			{
				_tcs.TrySetCanceled();
			}
		}
	}
}
