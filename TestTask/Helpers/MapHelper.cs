﻿using System;
using System.Collections.Generic;
using TestTask.Models;
using Xamarin.Forms.GoogleMaps;

namespace TestTask.Helpers
{
	public class MapHelper
	{
		public static List<Pin> GetPins(List<Site> sites)
		{
			if (sites == null)
				return null;

			var pins = new List<Pin>();

			foreach (var site in sites)
			{
				var pin = GetPin(site);
				if (pin == null)
					continue;

				pins.Add(pin);
			}

			return pins.Count == 0 
				? null 
				: pins;
		}

		public static Pin GetPin(Site site)
		{
			var position = GetPosition(site);
			if (position == null || IsEmptyPosition(position))
				return null;

			var pin = new Pin
			{
				Label = site.Title,
				Position = (Position) position,
				BindingContext = site
			};
			return pin;
		}

		public static Position? GetPosition(Site site)
		{
			return site?.Coordinate == null
				? null
				: GetPosition(site.Coordinate.Latitude, site.Coordinate.Longitude);
		}

		public static Position? GetPosition(double latitude, double longitude)
		{
			return new Position(latitude, longitude);
		}

		private const double Tolerance = 0.0001;

		public static bool IsEmptyPosition(Position? position)
		{
			return position == null || IsEmptyPosition(position.Value.Latitude, position.Value.Longitude);
		}

		public static bool IsEmptyPosition(double latitude, double longitude)
		{
			return Math.Abs(latitude) < Tolerance && Math.Abs(longitude) < Tolerance;
		}


		public static CameraUpdate GetCameraUpdate(Position? position, double zoom)
		{
			if (position == null || IsEmptyPosition(position))
				return null;
			var cameraPosition = GetCameraPosition(position, zoom);
			return CameraUpdateFactory.NewCameraPosition(cameraPosition);
		}

		public static CameraUpdate GetCameraUpdate(List<Site> sites)
		{
			if (sites == null)
				return null;

			var positions = new List<Position>();

			foreach (var site in sites)
			{
				var position = GetPosition(site);
				if (position == null)
					continue;

				positions.Add((Position) position);
			}

			if (positions.Count == 0)
				return null;

			var bounds = Bounds.FromPositions(positions);

			return GetCameraUpdate(bounds);
		}


		private const int DefaultBoundsPadding = 60;

		public static CameraUpdate GetCameraUpdate(Bounds bounds)
		{
			return bounds == null 
				? null
				: CameraUpdateFactory.NewBounds(bounds, DefaultBoundsPadding);
		}


		private const double DefaultZoom = 13;
		private const double DefaultBearing = 0;
		private const double DefaultTilt = 0;

		public static CameraPosition GetCameraPosition(Position? position,
			double zoom = DefaultZoom,
			double bearing = DefaultBearing,
			double tilt = DefaultTilt)
		{
			if (position == null || IsEmptyPosition(position))
				return null;
			return new CameraPosition((Position) position, zoom, bearing, tilt);
		}


		private const double DefaultPositionLatitude = 56.5;
		private const double DefaultPositionLongitude = 60.35;
		private const double DefaultPositionZoom = 0;

		public static CameraUpdate GetDefaultCameraUpdate()
		{
			var defaultPosition = new Position(DefaultPositionLatitude, DefaultPositionLongitude);
			var cameraPosition = GetCameraPosition(defaultPosition, DefaultPositionZoom);
			return CameraUpdateFactory.NewCameraPosition(cameraPosition);
		}
	}
}
