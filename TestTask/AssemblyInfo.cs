using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("FontAwesome5Pro-Light.ttf", Alias = "FaLight")]
[assembly: ExportFont("appian.ttf", Alias = "Appian")]