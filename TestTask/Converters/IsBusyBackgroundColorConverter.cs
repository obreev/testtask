﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace TestTask.Converters
{
	/// <summary>
	/// Skeleton не работает! :( Нее умеет управлять фоном, поэтому конвертер
	/// </summary>
	public class IsBusyBackgroundColorConverter : IValueConverter
	{
		#region Implementation of IValueConverter

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is bool result && parameter is Color color)
			{
				return result 
					? color 
					: Color.Transparent;
			}

			throw new ArgumentException("Value is not a valid boolean", nameof(value));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
